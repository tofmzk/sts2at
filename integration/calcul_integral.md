# Calcul intégrale

L'objectif ici n'est pas de faire un cours rigoureux mais de comprendre la notion d'intégrale et d'effectuer quelques calculs simples.



## Aire et intégrale



**Définition 1 :**

Soit $`f`$ une fonction définie, positive et continue sur un intervalle [a,b] de $`\R`$ avec $`a < b `$.

Comme nous l'avons vu dans le chapitre sur les lois continues, l'intégrale de $`f`$ entre $`a`$ et $`b`$, notée $`\int_{a}^{b}{f(x) dx}`$ est l'aire située entre la courbe de $`f`$ et l'axe des abscisses entre entre $`a`$ et $`b`$. L'intégrale, dans ce cas est donc positive.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Aire_sous_la_courbe.svg/800px-Aire_sous_la_courbe.svg.png" alt="intégrale d'une fonction positive" style="zoom:50%;" />

Sur cet exemple, $`\int_a^b {f(x) dx} = aire~bleue~`$



**Définition 2 :**

Soit $`f`$ une fonction définie, **négative** et continue sur un intervalle [a,b] de $`\R`$ avec $`a < b `$.

Comme nous l'avons vu dans le chapitre sur les lois continues, l'intégrale de $`f`$ entre $`a`$ et $`b`$, notée $`\int_{a}^{b}{f(x) dx}`$ est **l'opposée** de l'aire située entre la courbe de $`f`$ et l'axe des abscisses entre entre $`a`$ et $`b`$. L'intégrale, dans ce cas, est donc négative.

<img src="media/negative.jpg" alt="fonction négative et intégrale" style="zoom:50%;" />



Sur cet exemple, $`\int_a^b {f(x) dx} =~-~ aire~bleue~`$



**Définition 3 :**

Soit $`f`$ une fonction définie et continue sur un intervalle [a,b] de $`\R`$ avec $`a < b `$.

La fonction étant continue sur [a; b], on peut relever les intervalles sur lesquels la fonction est positive et ceux sur lesquels elle est négative.

On calcule  $`\int_{a}^{b}{f(x) dx}`$  en ajoutant  les aires situées entre la courbe de $`f`$ et l'axe des abscisses sur les intervalles où $`f`$ est positive et en soustrayant celles sur les intervalles où $`f`$ est négative.





<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Integral_example.png/600px-Integral_example.png" alt="intégrale d'un fonction et aire" style="zoom:50%;" />

Sur cet exemple, $`\int_a^b {f(x) dx} = aire~bleue~ -~aire~jaune`$



Allez, on va faire les exercices 1 et 2 de [cette fiche](exos.pdf)



## Intégrales et primitives



### Qu'est-ce qu'une primitive ?



**Propriété/définition 1** :

Toute fonction $`f`$ définie et continue sur un intervalle [a; b] admet une primitive $`F`$ sur cet intervalle, c'est à dire une fonction telle que, pour tout $`x \in [a;b]`$ on  $`F'(x) = f(x)`$.



**Exemples:**

* $`f(x)= 2`$ est définie et continue sur $`\R`$ et admet, sur  $`\R`$  , la fonction $`F(x) = 2x`$ pour primitive, puisque pour tout $`x \in \R`$ on  $`F'(x) = f(x)`$.
* $`f(x)= 2x`$ est définie et continue sur $`\R`$ et admet, sur  $`\R`$  , la fonction $`F(x) = ...`$ pour primitive, puisque pour tout $`x \in \R`$ on  $`F'(x) = f(x)`$.
* $`f(x)= x³`$ est définie et continue sur $`\R`$ et admet, sur  $`\R`$  , la fonction $`F(x) = ...$ pour primitive, puisque pour tout $`x \in \R`$ on  $`F'(x) = f(x)`$.



**Propriété 2 : cas des polynômes**

Tout polynôme est défini et continue sur  $`\R`$ et admet donc une primitive sur $`\R`$.



**Propriété 3 : **

Toute fonction  $`f`$  qui admet au moins une primitive $`F`$ sur un intervalle donné, en admet une infinité. En effet, pour toute constante $`k \in \R`$, $`F+k`$ est également une primitive de $`f`$.



### Primitives usuelles.

Pour bien diviser, il faut savoir multiplier.

De même pour déterminer une primitive, il faut savoir dériver.



Voici un petit tableau de primitives usuelles ( à connaître ! ) :

|       f       |   F    | sur I= |
| :-----------: | :----: | :----: |
|     $`k`$     | $`kx`$ | $`\R`$ |
|     $`x`$     |        | $`\R`$ |
|    $`x²`$     |        | $`\R`$ |
|    $`x³`$     |        | $`\R`$ |
|  $`e^{ax}`$   |        | $`\R`$ |
| $`\frac 1 x`$ |        |        |



**Exemple :**

Cherchons une primitive de $`f(x)= x² - 3x + 1`$

* Comme pour une dérivation, on cherche séparément une primitive de $`x²`$, $`-3x`$ et $`1`$ :
    * $`x²`$ admet pour primitive .....
    * comme pour une dérivation, le coefficient _-3_ ne _bougera_ pas. Une primitive de $`-3x`$ est donc .....
    * pour terminer, 1 admet pour primitive : 
* Ainsi $`F(x)=`$



Cherchons une primitive de $`f(x)= 3e^{2x}`$

* Le 3 ...
* Une primitive de $`e^{2x}`$ est ....
* $`F(x)=`$









### Quel rapport avec le calcul intégrale ?



**Propriété :**

Soit une fonction  $`f`$  qui admet une primitive $`F`$ sur un intervalle [a; b]. Alors : 

$`\int_a^b {f(x) fx} = F(b) - F(a)`$

On note également :

$`\int_a^b {f(x) fx} =[F(x)]_a^b`$`



**Exemples :**



Reprenons nos calculs de primitives précédentes pour calculer les intégrales ci-dessous :

* $`\int_0^1 {x² - 3x + 1 dx}`$
* $`\int_0^1 {3e^{2x}}`$





Il est temps d'aller faire les exercices restants sur [cette fiche !](exos.pdf)





__________

Par Mieszczak Christophe

Licence CC BY SA



Sources images :

* [intégrale d'une fonction positive - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:Aire_sous_la_courbe.svg)
* Intégrale d'une fonction négative : production personnelle à partir de l'image précédente.
* [intégrale et aire - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:Integral_example.png)