# Statistiques inférentielles - Intervalles de confiance.

#### Objectif



A partir de l'observation d'un échantillon, on veut tirer des conclusions sur la population entière avec une marge d'erreur définie.

Pa exemple, si dans un échantillon d'un certaine taille n de la population 53% des sondés affirment vouloir coter pour un candidat, dans quelle intervalle peut-on estimer que la population complète va voter, avec un confiance de 95% ?



## Estimation ponctuelle : un peu de naïveté.



_Exemple :_

On prélève au hasard dans une population de taille importante, un échantillon de 36 pièces dont on mesure les masses. On considère qu'une pièce est défectueuse si sa masse est inférieure strictement à 755g ou strictement supérieure à 795 g. Voici les résultats :



| Masses 		des pièces (en grammes) | Nombre 		de pièces |
| -------------------------------------- | ------------------------ |
| [745 ; 		755[                    | 2                        |
| [755 ; 		765[                    | 6                        |
| [765 ; 		775[                    | 10                       |
| [775 ; 		785[                    | 11                       |
| [785 ; 		795[                    | 5                        |
| [795 ; 		805[                    | 2                        |

​       

* A la calculatrice, pour cet échantillon, calculez :
    * la moyenne $$\overline x =$$
    * L'écart-type $$\sigma ' = $$
    * la fréquence de pièce défectueuse $$f = $$



> Soit un échantillon parfaitement connu de taille n, de moyenne $$\overline x$$ et d’écart type $$\sigma  ’$$ dont une fréquence $$f$$ possède une certaine propriété dans une population.
>
> 
>
> - On choisit comme **estimation** **ponctuelle** de la moyenne $$\mu$$ de la population la moyenne de l’échantillon : $$\mu = \overline x$$
> - On choisit comme **estimation** **ponctuelle** de l’écart type  de la population le nombre $$\sigma = \sigma ' \times \sqrt \frac {n}{n - 1}$$
> - On choisit comme **estimation** **ponctuelle** de la proportion $$p$$ de la population possédant la même propriété $$p=f$$.



_Revenons à l'exemple :_

- On choisit comme **estimation** **ponctuelle** de la moyenne $$\mu$$ de la population la moyenne de l’échantillon : $$\mu = \overline x = $$
- On choisit comme **estimation** **ponctuelle** de l’écart type  de la population le nombre $$\sigma = \sigma ' \times \sqrt \frac {n}{n - 1} = $$
- On choisit comme **estimation** **ponctuelle** de la proportion $$p$$ de la population possédant la même propriété $$p=f=$$ .



_Remarques :_

* Il s'agit ici d'une précision naïve : on considère que la population se comportera comme l'échantillon.
* Pour tout n entier naturel, $$n>n-1$$ donc $$\frac {n}{n-1} > 1$$ donc $$\sigma$$ est toujours supérieur à $$\sigma$$.
* Lorsque $$n$$ devient très grand, $$\sqrt \frac {n}{n-1}$$ se rapproche de ... : l'écart-type de la population $$\sigma = \sigma ' \times \sqrt \frac {n}{n - 1}$$tend donc vers celui de l'échantillon.



## Intervalle de confiance et fréquence.



De manière un peu moins naïve, si, dans un échantillon de taille n (assez grand), une fréquence $$f$$ possède une certaine propriété, on peut penser que, dans la population, la proportion $$p$$ d'individus qui posséderont cette même propriété va ''tourner'' autour de $$f$$.

Ici, on veut trouver un intervalle, appelé **intervalle de confiance** auquel la proportion $$p$$ a une certaine probabilité $$\alpha \in ]0;1[$$ , choisie arbitrairement, d'appartenir.



_Exemple :_ 

Plus haut, notre échantillon de taille $$n = 36$$ présentait une fréquence de pièces défectueuses $$f=$$.

A quel intervalle de confiance la proportion $$p$$ de pièces défectueuses de la population a-t-elle 95% de chance d'appartenir ?



> Soit un échantillon de taille n assez grand (n>30) dont une fréquence $$f$$ possède une certaine propriété. Soit $$p$$ la proportion d'individus possédant la même propriété dans la population.
>
> * On choisit un **coefficient de confiance** $$\alpha \in ]0;1[$$.
>
> * On détermine $t \in \R^+$ tel que $$p(-t \leq T \leq t) = \alpha$$ où $$T \rightarrow N(0;1)$$.
>
>      
>
>     Alors il y a une probabilité $$\alpha$$ pour que la $$p$$ appartienne à l'intervalle de confiance :
>
>     $$I_\alpha = [f - t \sqrt \frac {f(1-f)}{n-1}; f + t \sqrt \frac {f(1-f)}{n-1}]$$
>
>     



_Revenons à l'exemple :_

* $$\alpha = $$

* on doit chercher $t \in \R^+$ tel que $$p( -t \leq T \leq t) = $$ 

    La calculatrice nous répond $$t=$$

    Alors il y a une probabilité $$\alpha$$ pour que la $$p$$ appartienne à l'intervalle de confiance :

    $$I_\alpha = [f - t \sqrt \frac {f(1-f)}{n-1}; f + t \sqrt \frac {f(1-f)}{n-1}]$$

    $$I_\alpha = [... - ... \sqrt \frac {...(1-...)}{...-1}; ... + ... \sqrt \frac {...(1-...)}{...-1}]$$

    $$I_\alpha = [.... ; ....]$$

    

## Intervalle de confiance et moyenne



De manière un peu moins naïve, si un échantillon de taille n (assez grand) a une moyenne $$\overline x$$ et un écart_type $$\sigma '$$,on peut penser que, dans la population, la moyenne $$\mu$$ va ''tourner'' autour de $$\overline x$$ avec un écart-type un peu plus grand que $$\sigma '$$.

Ici, on veut trouver un intervalle, appelé **intervalle de confiance** auquel la moyenne $$\mu$$ a une certaine probabilité $$\alpha \in ]0;1[$$ , choisie arbitrairement, d'appartenir.



_Exemple :_ 

Plus haut, notre échantillon de taille $$n = 36$$ présentait une moyenne $$\overline x =$$ et un écart-type $$\sigma ' = $$ 

A quel intervalle de confiance la moyenne $$\mu$$  de la population a-t-elle 97% de chance d'appartenir ?





> Soit un échantillon de taille n assez grand (n>30) de moyenne $$\overline x$$ et d'écart_type $$\sigma '$$. Soit $$\mu$$ la moyenne de la population et $$\sigma$$ son écart-type.
>
> * On choisit un **coefficient de confiance** $$\alpha \in ]0;1[$$.
>
> * On détermine $t \in \R^+$ tel que $$p(-t \leq T \leq t) = \alpha$$ où $$T \rightarrow N(0;1)$$.
>
> * **si $$\sigma$$ n'est pas connu** alors on prend pour estimation ponctuelle $$\sigma = \sigma ' \times \sqrt \frac {n}{n - 1} $$
>
>      
>
>     Alors il y a une probabilité $$\alpha$$ pour que la $$\overline x$$ appartienne à l'intervalle de confiance :
>
>     $$I_\alpha = [\overline x - t \frac {\sigma}{\sqrt{n-1}}; \overline x + t \frac {\sigma}{\sqrt{n-1}}]$$
>
>     
>
>     

_Revenons à l'exemple :_

* $$\alpha = $$

* on doit chercher $t \in \R^+$ tel que $$p( -t \leq T \leq t) = $$ 

    La calculatrice nous répond $$t=$$

* On a déjà calculé plus haut l'estimation ponctuelle $$\sigma = \sigma ' \times \sqrt \frac {n}{n - 1} $$ =



​		Alors il y a une probabilité $$\alpha$$ pour que la $$\overline x$$ appartienne à l'intervalle de confiance :

​		Alors il y a une probabilité $$\alpha$$ pour que la $$\overline x$$ appartienne à l'intervalle de confiance :

​		$$I_\alpha = [\overline x - t \frac {\sigma}{\sqrt{n-1}}; \overline x + t \frac {\sigma}{\sqrt{n-1}}]$$

​		$$I_\alpha = [... - ... \frac {...}{\sqrt{...-1}}; ... + ... \frac {...}{\sqrt{...-1}}]$$

​		$$I_\alpha = [... ; ...]$$





______

Par Mieszczak Christophe

Licence CC BY SA