# Test d'hypothèse



### Objectif



Une hypothèse, appelée **hypothèse nulle** et notée $$H_0$$, est faite au sujet d'une population complète, concernant une moyenne ou une proportion. Cette hypothèse est invérifiable de façon certaine car on ne peut pas la calculer sur l'entièreté de la population: Il nous faut mettre en place un test pour nous aider à décider si on peut croire ou non cette hypothèse, avec un certain risque d'erreur.

_Exemple :_

Un constructeur annonce un poids moyen de $$\mu=$$  780g et un maximum de 9% de pièces défectueuses pour la production totale d'une pièce donnée : c'est l'hypothèse nulle. Peut-on le croire avec un risque de 5% ?



### Retour à l'échantillonnage



Le principe du test d'hypothèse est celui de la démonstration par l'absurde.

* On suppose une hypothèse vraie.
* On réalise une série de déductions logiques à partir de cette hypothèse et :
    * si on parvient à une déduction impossible ,on en déduit que l'hypothèse de départ était fausse.
    * sinon, **on n'a pas réussi à prouver que l'hypothèse est fausse** mais **on ne peut être certain qu'elle est vraie pour autant**.  



**Le test d'hypothèse :**

* On suppose $$H_0$$ vraie : On suppose donc qu'on connaît, pour une population sa moyenne $$\mu$$ ou une proportion $$p$$ d'un caractère.

* On choisit un coefficient de confiance $$\alpha \in ]0;1[$$.

* On va déterminer **l'intervalle de fluctuation** où devrait se situer la moyenne $$\overline x$$ (ou la fréquence $$f$$ du caractère étudié) d'un échantillon de taille $$n$$ où $$n$$ est choisi arbitrairement :

    * On cherche $$t \in \R^+$$ tel que $$p(-t \leq T \leq t ) = \alpha$$ avec $$T \rightarrow N(0;1)$$ 
    * Grâce à ce que nous savons sur l'échantillonnage on peut affirmer qu'il y a une probabilité $$\alpha$$ pour que  :
        * si on travaille sur une moyenne, $$\overline x \in [\mu - t \frac {\sigma}{\sqrt n} ; \mu + t \frac {\sigma}{\sqrt n}]$$ où $$\sigma$$ est l'écart-type de la population.
        * si on travaille sur une proportion, $$f \in [p - t \sqrt\frac {p (1-p)}{n} ; p + t \sqrt \frac {p (1-p)}{n}]$$

* On extrait maintenant un échantillon de la taille $$n$$ choisie précédemment. On peut mesurer, sur cet échantillon, soit sa moyenne $$\overline x$$ soit la fréquence $$f$$ du caractère choisi :

    * si la grandeur mesurée **n'appartient pas** à l'intervalle de confiance calculé plus haut, alors il y a une probabilité $$\alpha$$ pour que l'hypothèse de départ soit fausse : **On refuse l'hypothèse au risque de $$1-\alpha$$.**
    * sinon, on n'a pas pu prouver que l'hypothèse était fausse : on l'accepte donc, mais on ne connaît pas le risque pris dans ce cas.

    

_Reprenons notre exemple :_



**Commençons par la moyenne**.



Un constructeur annonce un poids moyen de $$\mu$$ = 780g pour la production totale d'une pièce donnée : c'est l'hypothèse nulle. Peut-on le croire avec un risque de 5% ?



* $$H_0$$ = 780  et $$\alpha=$$0.95.

* **On suppose $$ H_0$$ vraie**

* On cherche  $$t \in \R^+$$ tel que $$p(-t \leq T \leq t ) = 0.95$$ avec $$T \rightarrow N(0;1)$$ : on trouve $$t=$$ 1,96

* Si un échantillon de taille $$n=36$$ de la production à pour moyenne $$\overline x$$ alors on peut affirmer qu'il y a une probabilité $$\alpha=$$0.95 pour que  $$\overline x \in [\mu - t \frac {\sigma}{\sqrt n} ; \mu + t \frac {\sigma}{\sqrt n}]$$ où $$\mu$$ est la moyenne, c'est à dire 780, et $$\sigma$$ est l'écart type de la population. Comme on ne connaît pas $$\sigma$$, on va prendre une estimation ponctuelle de l'écart-type de l'échantillon afin de déterminer cet intervalle.

* Voici mon échantillon de taille 36 :

  ​    

    | Masses des pièces (en grammes) | Nombre de pièces |
    | :----------------------------: | :--------------: |
    |      [745 ; 		755[       |        2         |
    |      [755 ; 		765[       |        6         |
    |      [765 ; 		775[       |        10        |
    |      [775 ; 		785[       |        11        |
    |      [785 ; 		795[       |        5         |
    |      [795 ; 		805[       |        2         |

    ​      Sur cet échantillon : $$\overline x = 774, 72$$ et $$\sigma_e = 12,36$$

    

* L'estimation ponctuelle de l'écart-type de la population est $$\sigma = \sigma_e \times \sqrt { \frac {n} {n-1}}$$ = $$12.36 \times \sqrt {\frac {36}{35}}=$$ 12.54

* Donc notre intervalle de confiance est :

     $$[\mu - t \frac {\sigma}{\sqrt n} ; \mu - t \frac {\sigma}{\sqrt n}]=$$ $$[780 - 1,96 \times \frac {12,54}{\sqrt 36};780 + 1,96 \times \frac {12.54}{\sqrt 36}]=$$ [775,9 ; 784,1]

* Décision :

	* Dans l'échantillon prélevé, $$\overline x = 774,72$$ donc $$\overline x \notin [775,9 ; 784,1]$$ :

		```txt
		On refuse de croire l'hypothèse nulle au risque 5% 
		```

		

**Étudions maintenant la proportion**.

Un constructeur annonce un maximum de 9% de pièces défectueuses pour la production totale d'une pièce donnée, c'est à dire une proportion de pièces défectueuses est $$p = 0.09$$ : c'est l'hypothèse nulle. Peut-on le croire avec un risque de 5% ?



* $$H_0$$ = 0.09  et $$\alpha=$$0.95. 

* **On suppose $$ H_0$$ vraie.**

* On cherche  $$t \in \R^+$$ tel que $$p(-t \leq T \leq t ) = \alpha$$ avec $$T \rightarrow N(0;1)$$ : on trouve $$t=$$1.96

* Si un échantillon de taille $$n=36$$ de la production a une fréquence de pièces conformes $$f$$ alors il y a une probabilité $$\alpha=0.95$$ pour que $$f \in [p - t \sqrt\frac {p (1-p)}{n} ; p + t \sqrt \frac {p (1-p)}{n}]$$

    C'est à dire $$[0.09 - 1.96 \times \sqrt {\frac {0.09 \times 0.91}{n}}, 0.09 + 1.96 \times \sqrt {\frac {0.09 \times 0.91}{n}}]=$$ $$[0, 0.183 ]$$

* Dans l'échantillon de taille 36 prélevé plus haut, la fréquence de pièces défectueuses est  $$f = \frac 1 9$$ :

    * $$\frac 1 9 \approx 0.11$$ donc $$f \in [0, 0.183]$$ 

        ````txt
        On n'a pas pu prouver que l'hypothèse était fause donc on l'accepte. 
        (ici on ne connait pas le risque pris)
        ````

        

        

### Application

**Ex 1:**

Un fabriquant fourni des pièces cylindriques. Il annonce que le diamètre moyen de pièces de sa production est $$\mu = 5cm$$. On se demande si on peut le croire avec un risque 3%.

* $$H_0 = 5$$ et $$\alpha=0.97$$

* On suppose que $$H_0$$ est vraie.

* Si on prélève un échantillon de 50 pièces alors la moyenne $$\overline x$$ de cet échantillon à un probabilité 0.97 d'être dans l'intervalle de confiance :

	 $$I = [\mu - t \frac {\sigma}{\sqrt n} ; \mu + t \frac {\sigma}{\sqrt n}]$$ où :

	* $$\sigma$$ est l'écart-type de la population (ici inconnu, on aura besoin d'une estimation ponctuelle).
	* $$\mu$$ est la moyenne de la population, c'est à dire 5 (c'est $$H_0$$ !)
	* $$t$$ est le réel positif tel que  $$p(-t \leq T \leq t ) = 0.97$$ avec $$T \rightarrow N(0;1)$$ : on trouve $$t=2.17$$ 

	

* On prélève un échantillon de 50 pièces :

	| Diamètre 		( en cm ) | 4,97 | 4,98 | 4,99 | 5    | 5,01 | 5,02 | 5,03 |
	| -------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
	| Effectif                   | 1    | 0    | 13   | 18   | 17   | 0    | 1    |



​		Sur cet échantillon : $$\overline x=$$ 5.001 et $$\sigma_e=$$ 0.0098



* L'estimation ponctuelle de l'écart-type de la population est $$\sigma = \sigma_e \times \sqrt { \frac {n} {n-1}}$$ = $$0.0098 \times \sqrt {\frac {50}{49}}=$$ 0.0099

* Donc notre intervalle de confiance est :

	 $$I = [\mu - t \frac {\sigma}{\sqrt n} ; \mu - t \frac {\sigma}{\sqrt n}]=$$ $$[5 - 2.17 \times {\frac {0.0099}{50}}; 5 + 2.17 \times {\frac {0.0099}{50}}]=$$  [ 4.997 ; 5.003 ]



**Conclusion :**

$$\overline x \in I$$ : on n'a pas réussi à prouver que l'hypothèse était fausse donc **on accepte $$H_0$$!**





