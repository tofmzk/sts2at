# Lois continues : Introduction



## Préambule : expérimentons



### Probabilités discrètes.

Lorsqu'on lance un dé à six faces non truqué, les résultats possibles de l'expérience aléatoire sont aisés à dénombrer : il y en a 6 qui son 1-2-3-4-5 et 6. Une telle expérience, où les événements sont dénombrables, est appelée expérience `discrète` . La loi d'équiprobabilité, très intuitive, nous dit que la probabilité d'obtenir 6, par exemple, et de une chance (cas favorable) sur 6 (nombre de possibilités totales), soit $`\frac 1 6`$.



### Choix d'un nombre réel dans [0;1].

On demande à trois élèves de choisir X un nombre au hasard dans l'intervalle [0;1]. `Au hasard` signifie qu'il ne doit pas être possible de prévoir à l'avance quel nombre il vont choisir. 

La différence avec le lancé de dé vu plus haut, c'est qu'entre 0 et 1, il y a une infinité de nombres et que nous, êtres humains, ne pouvons écrire tous ces nombres, ne serait-ce que parce qu'ils peuvent avoir une infinité de chiffres dans leur partie décimale. Les élèves décident donc de se limiter à 16 chiffres après la virgules , ce qui leur donne le choix entre $`10^{16} `$ nombres au lieu d'une infinité. Par rapport à l'infini, c'est peu. Cependant cela fait déjà beaucoup de nombres : $`10^{16} = 10 \times 10^6 \times 10^9`$ , soit dix millions de milliards de choix possibles.



On ne peut pas, comme c'est le cas lors d'une expérience discrète, noter chaque valeur possible car il y en a trop. 

La probabilité lorsqu'on prédise, à l'avance, le nombre choisi par un des élèves est de $`\frac 1 {10^{16}}= 0.0000000000000001`$ . Ca ne fait pas beaucoup... et elle serait de 0 s'il était possible de choisir X sans se limiter à 16 chiffres après la virgule.

 On ne va donc pas regarder la probabilité qu'à un nombre de sortir, mais celle qu'il tombe dans un intervalle.



Allons-y, simulons donc les tirages aléatoires de nos trois élèves en suivant [ce lien](simulateur/simulateur.html).



On remarque que :

* Chacun des élèves choisit bien aléatoirement car il est impossible de prédire le résultat de ce choix avec certitude.

      

* Si on n'effectue que quelques tirages, il est impossible de distinguer quoi que ce soit, tout semble très chaotique.

      

* En revanche, sur un grand nombre de tirages, on s'aperçoit que l'histogramme de la répartition des fréquences semble suivre une _tendance_ : il est possible de trouver une fonction $`f`$ dont la courbe épouse l'histogramme, et ce même en répétant, autant de fois qu'on le veut, les séries de tirages.

<img src="/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/jean.jpg" alt="jean" style="zoom:50%;" />

<img src="/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/sylvie.jpg" alt="Sylvie" style="zoom:50%;" />

<img src="/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/yan.jpg" alt="Yan" style="zoom:50%;" />





* Chaque élève choisit au hasard un nombre dans [0;1], mais avec une certaine _stratégie_ :
    * Yan, choisit de façon `uniforme` : il y a autant de chance que le nombre _tombe_ dans chaque intervalle.
    * Jean, en réalité, procède comme Yan, puis met le résultat qu'il obtient au carré. En conséquence, les nombres sont plus souvent plus proches de 0 que de 1 car, $`si~x \in [0 ; 1]~alors~x² \leq x`$.
    * Sylvie a tendance a choisir des nombres autour de la moyenne 0.5 qu'aux extrémité 0 et 1. Nous verrons qu'il s'agit là d'une répartition `normale`.



### Du discret au continue



On souhaite maintenant essayer de prédire la probabilité que le prochain nombre a de tomber dans un intervalle. 

* On note que la probabilité pourrait être approchée par la fréquence de cet intervalle, lorsque le nombre d'expériences devient très très très très grand. Par exemple, dans le cas de Yan, la fréquence d'apparition d'un nombre dans l'intervalle [0,55; 0,60] serait de 0,051 environ (cf l'image juste au dessus, dernière ligne du tableau). 

* Cette fréquence, graphiquement, correspond à l'aire du rectangle de l'histogramme pour cet intervalle, peinte en bleu foncé ci-dessous. Le rectangle mesure 0.05 de large sur 1.02 (approximativement, regardez l'axe des ordonnées) de haut est à donc une aire de $`0,05 \times 1,02 = 0,051`$ : cela correspond bien à notre fréquence !

    <img src="/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/yan_2.jpg" alt="yan_2" style="zoom:50%;" />



* Par exemple, $`p(0.2 \leq X \leq 0.5)`$ est égale à l'aire du rectangle bleu foncé ci-dessous, c'est à dire à la somme des aires des 6 recatngles de cet intervalle : $`0,049 + 0,051 + 0,050 + 0,048+ 0,050 + 0,050 \approx 0,3`$

    <img src="/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/yan_3.jpg" alt="yan_3" style="zoom:50%;" />

    

* Connaissant la fonction $`f(x) = 1`$ qui _épouse_ notre histogramme(regardez sur le graphique ci-dessus), on pourrait _modéliser_ une loi de probabilité pour laquelle la probabilité qu'a X d'appartenir à un intervalle donné est l'aire située, dans cet intervalle, entre l'axe des abscisses et la courbe de $`f`$.  Par exemple, $`p(0.2 \leq X \leq 0.5) = 1 \times 0.3 = 0.3 `$ 

      



* Pour plus de précision, il faudrait diminuer la largeur des intervalles. Pour cela il faudrait en augmenter le nombre. Notre simulateur permet de prendre 10 intervalles (de largeur $`\frac 1 {10}`$) ou 20 intervalles (de largeur $`\frac 1 {20}`$). Il faudrait en prendre beaucoup plus : 100 (de largeur $`\frac 1 {100}`$), 1000 (de largeur $`\frac 1 {1000}`$), .... Dans l'idéal, le nombre d'intervalles devrait tendre vers $`\infty`$ et, alors, la largeur de nos rectangle tendrait, elle, vers 0.

    Pour calculer notre aire, il faudrait ajouter un nombre infini d'aires de rectangles. Chaque rectangle, s'il est situé à l'abscisse $`x`$, aurait pour hauteur $`f(x)`$, une largeur infinitésimale notée $`dx`$ et donc une aire $`f(x) dx`$. Nous passons alors d'une somme `discrète` à une somme continue, qu'on appelle `intégrale`. 

    On note ainsi $`\int_{a}^{b}{f(x)dx}`$, c'est à dire `intégrale` (somme continue) de $`f(x) dx`$, pour $`x`$ allant de _a_ à _b_, l'aire en question.

    Nous sommes passé d'une somme discrète à une somme continue.



## Loi continue



### Définition

On dit qu'une variable aléatoire X, qui prend ses valeurs dans un intervalle $`I`$ de $`\R`$ , suit une loi continue, s'il existe une fonction $`f`$, appelée densité de la loi, telle que :

* $`f`$ est définie et continue sur $`I`$.
* pour tout $`x \in I, f(x) \ge 0`$
* sur$`I`$, l'aire totale comprise entre la courbe de $`f`$ est l'axe des abscisses vaut 1.
* Pour tout $`a`$ et$`b`$ de $`I`$, avec $`a \le b`$, $`p(a \ge X \ge b) = \int_{a}^{b} {f(x) dx}`$



_Exemple_ :

Si nous reprenons le cas de Yan, la variable aléatoire $`X \in [0; 1]`$, correspondant au nombre qu'il choisit , suit une loi continue de densité $`f(x) = 1`$ sur $`I=[0; 1]`$.



_Remarque :_

si X suit une loi continue de densité $`f`$  sur $`I`$, alors, pour tout $`a \in I`$ ,$` p(X = a) = p(a \le X \le a) =\int_{a}^{a} {f(x) dx} = 0`$

Autrement dit, entre les abscisses $`a`$ et $`a`$, il n'y a pas d'aire.