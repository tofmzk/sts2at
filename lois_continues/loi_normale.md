# Loi Normale



### Définitions



On dit qu'une variable aléatoire X à valeurs dans $`\R`$ suit une loi normale de paramètre $`\mu`$ et $`\sigma`$ si et seulement si sa densité de probabilité est  

$`f(x)= \frac {1} {\sigma \sqrt {2 \pi }} e^{-\frac 1 2(\frac {x - m} {\sigma})^2}`$. 

Alors :

* on note $`X \rightarrow N(\mu, \sigma )`$
* $`E(X) = \mu`$ et $`\sigma(X) = \sigma`$.

* La courbe représentative de la densité est appelée `courbe de Gauss` ou `courbe en cloche`. Elle possède un axe de symétrie d'équation $`x = \mu`$.

![courbe de Gauss](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/normale1.jpg)

* Si l'écart-type augmente, la courbe _s'élargit_ :

    ![courbe de Gauss](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/normale2.jpg)

* Si la moyenne augmente, la courbe se décale vers la droite.

![courbe de Gauss](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/normale4.jpg)

* Si la moyenne diminue, la courbe se décale vers la gauche.

![courbe de Gauss](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/normale_3.jpg)

### Calculer avec la loi normale

Si $`X \rightarrow N(\mu, \sigma)`$ alors $`p(a \leq X \leq b) ={\frac {1} {\sigma \sqrt {2 \pi }} \int_a^b  e^{-\frac 1 2(\frac {x - m} {\sigma})^2}}`$.

On ne peut calculer cette intégrale en valeur exacte : nous allons devoir utiliser la calculatrice et donner des résultats en valeurs approchées.

C'est parti pour les exercices 1, 2, 3 et 4 de [cette fiche](loi_normale_exos.pdf).

### Loi normale et loi binomiale



Soit $`X \rightarrow B(n, p)`$.

Si le conditions suivantes sont toutes réunies :

* $`n \geq 30`$
* $`np \geq 5`$
* $`n(1-p) \geq 5`$

Alors $`X`$ loi de $`X`$ peut être approximée par la loi normale $`N(E(X), \sigma(X))`$ avec :

* $`E(X) = np`$
* $`\sigma(X) = \sqrt {np(1-p)}`$

_Remarque : Cela découle du théorème de `Moivre-Laplace` qui date du début du 19ème siècle_.



Allez, on retourne voire [cette fiche](loi_normale_exos.pdf) et l'exercice 5.

### Loi normale centrée réduite



Lorsque $`X \rightarrow N(0,1)`$ , on dit que X suit la loi `normale centrée`. On a alors :

* $`f(x)=\frac 1 {2 \pi}e^{-\frac 1 2 x²}`$
* $`p(a \leq X \leq b) ={\frac {1} {\sqrt {2 \pi }} \int_a^b  e^{-\frac 1 2 x²}}`$.

​    

![courbe de Gauss centrée](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/normale_centree.jpg)

**Propriété : changement de variable**

Si $`X \rightarrow N(\mu, \sigma)`$ alors la variable $`T = \frac {X - \mu} {\sigma}`$ suit la loi normale centrée réduite.

Ainsi, on peut ramener toute loi normale à la loi centrée réduite par changement de variable.

**Conséquence (démontrée en exercice)**:

Si $`X \rightarrow N(\mu, \sigma)`$  alors :

* $`p(\mu - \sigma \leq X \leq \mu + \sigma ) \approx 0.68`$
* $`p(\mu - 2\sigma \leq X \leq \mu + 2\sigma ) \approx 095`$
* $`p(\mu - 3\sigma \leq X \leq \mu + 3\sigma ) \approx 0.997`$

Ces valeurs particulières sont importantes et doivent être connues. Nous en reparlerons souvent.





Allons le voir dans les exercices suivants de [notre fiche](loi_normale_exos.pdf).



___

Mieszczak Christophe CC BY SA