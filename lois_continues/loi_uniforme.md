# Loi uniforme



Nous l'avons évoqué plus haut, le cas de Yan est un cas `uniforme`. Dans ce cas, la densité est constante.

Pour la trouver, il suffit de se servir du fait que l'aire totale entre l'axe des abscisses et la courbe, sur l'intervalle, doit valoir 1.

Dans le cas de Yan, puisque l'intervalle [0; 1] à pour largeur 1, la hauteur du rectangle doit également être de 1 de façon à ce que l'aie total fasse $`1 \times 1 = 1`$.



### Définition

On dit qu'une variable aléatoire X, qui prend ses valeurs dans un intervalle [m, n] de $`\R`$ ,  (avec m < n), suit une loi uniforme,  si elle admet pour densité sur cet intervalle $`f(x)=\frac 1 {n - m}`$. Cette loi uniforme est notée U(m, n).



![uniforme](/home/t450/Documents/Boulot/lycée/sts2at/lois_continues/media/uniforme.jpg)



_Exemple :_ 

Soit X une variable aléatoire de loi U(1, 5).

* Quelle sera la densité de X ?
* calculez :
    * $`p(1 \le X \le 2)`$
    * $`p(X \ge 1)`$
    * $`p(X = 1)`$

### Exercices



Vous trouverez [quelques exercices en cliquant ici](exercices.pdf)



### Espérance et écart-type

Soit X une variable aléatoire suit une loi uniforme de densité $`f`$ sur un intervalle [m; n] (avec m < n).

Alors :

* L'espérance de X est $`E(X) = \frac {m+n} 2`$
* L'écart-type de X est $`\sigma (X) = \frac {n - m} {2 \sqrt 3}`$

