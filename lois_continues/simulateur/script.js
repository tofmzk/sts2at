////////////////////////////////////////
//VARIABLES GLOBALES///////////////////
///////////////////////////////////////
var Eff = new Array();
var Freq = new Array();
var Total = 0;
var Eleve="Jean";
var Racine=new Image();
var Normal=new Image();
var NbreIntervalle=10;


/////////////////////////////////////////
///// abonnements///////////////////////
////////////////////////////////////////

// apr�s chargement
function setupEvents () { 
// abonnements pas de VAR pour avoir des variables globale
    document.getElementById("UnNombre").addEventListener("click",Choisit);
	document.getElementById("CentNombres").addEventListener("click",ChoisitCent);
	document.getElementById("Dix").addEventListener("click",ChoisitDix);
	document.getElementById("Vingt").addEventListener("click",ChoisitVingt);
	
	document.getElementById("EleveJean").addEventListener("click",Jean);
	document.getElementById("EleveSylvie").addEventListener("click",Sylvie);
	document.getElementById("EleveYan").addEventListener("click",Yan);
	
	Racine.src="racine.jpg";
	Normal.src="normal.jpg";
	Initialise();

	}


//////////////////////////////////////////
/////////Initialisation //////////////
//////////////////////////////////////////

function Initialise(){
	for (var i=0;i<20;i++){
		Eff[i]=0;
		Freq[i]=0;
		Total=0;
		document.getElementById("E"+(i+1)).innerHTML="0";
		document.getElementById("F"+(i+1)).innerHTML="0";
	}
	if (NbreIntervalle==10) {
		document.getElementById("Dix").checked=true;
		for (var i=1;i<11;i++){
			document.getElementById("L"+i).style.visibility="hidden";
			document.getElementById("I"+i).innerHTML="["+((i-1)/10)+";"+(i/10)+"[";
		}
	}
	if (NbreIntervalle==20) {
		document.getElementById("Vingt").checked=true;
		for (var i=1;i<21;i++){
			if (i<11) {document.getElementById("L"+i).style.visibility="visible";}
			document.getElementById("I"+i).innerHTML="["+((i-1)/20)+";"+(i/20)+"[";
			
		}
	}
		document.getElementById("EffTotal").innerHTML="0";
	document.getElementById("FreqTotale").innerHTML="0";
	document.getElementById("Action").innerHTML="Hum...."
	miseajourcanvas();
}

function ChoisitDix(){
	NbreIntervalle=10;
	Initialise();
}
	
function ChoisitVingt(){
	NbreIntervalle=20;
	Initialise();
}

//////////////////////////////////////////
/////////choix des nombres  //////////////
//////////////////////////////////////////

function ChoisitCent(){
	document.getElementById("Action").innerHTML="Hum.... Je choisis cent nombres d'un coup .." // je choisis ....
	for (var cent=0;cent<100;cent++) {	
		var nombre=-1;
		while ((nombre<0)||(nombre>1)){
			nombre=Hasard();
		}
		var n=Math.floor(nombre*NbreIntervalle);
		Eff[n]++;
	}
	Total=0;
	for (var i=0;i<NbreIntervalle;i++) { 
		Total=Total+Eff[i];
	}
	for (var i=0;i<NbreIntervalle;i++) { 
		Freq[i]=Math.round(1000*Eff[i]/Total)/1000;
		document.getElementById("F"+(i+1)).innerHTML=Freq[i]; // mise � jour fr�quence
		document.getElementById("E"+(i+1)).innerHTML=Eff[i]; // mise � jour effectif
	}
	document.getElementById("EffTotal").innerHTML=Total; //mise � jour effectif total
	document.getElementById("FreqTotale").innerHTML="1"; //mise � jour effectif total
	miseajourcanvas();
}


function Choisit(){
	var nombre=-1;
	while ((nombre<0)||(nombre>1)){
		nombre=Hasard();
	}
	var n=Math.floor(nombre*NbreIntervalle);
	Eff[n]++;
	Total=0;
	for (var i=0;i<NbreIntervalle;i++) { 
		Total=Total+Eff[i];
	} // calcul effectif total;
	for (var i=0;i<NbreIntervalle;i++) { 
		Freq[i]=Math.round(100*Eff[i]/Total)/100;
		document.getElementById("F"+(i+1)).innerHTML=Freq[i]; // mise � jour fr�quence
		document.getElementById("E"+(i+1)).innerHTML=Eff[i]; // mise � jour effectif
	}
	document.getElementById("EffTotal").innerHTML=Total; //mise � jour effectif total
	document.getElementById("FreqTotale").innerHTML="1"; //mise � jour effectif total
	document.getElementById("Action").innerHTML="Hum.... Je choisis  " + nombre ; // je choisis ....
	miseajourcanvas();
}

function Hasard(){
	var Nbre;
	if (Eleve=="Jean"){
		var Nbre=Math.random();
		Nbre=Nbre*Nbre;
		return Nbre;
	}
	if (Eleve=="Sylvie"){
		while ((Nbre<0)||(Nbre==null)) {
			var X1=Math.random();
			var X2=Math.random();
			if (X1==0) {X1=0.01};
			var Y1=Math.sqrt(-2*Math.log(X1))*Math.cos(X2*Math.PI);
			Nbre=0.5+0.2*Y1; // n(0.5,0.2)
		}
		return Nbre;
	}

	
	if (Eleve=="Yan"){
		Nbre=Math.random();
		return Nbre;
	}
	
	
}



/////////////////////////////////////////
//// choix de l'�l�ve //////////////////
//////////////////////////////////////
function Jean(){
	Eleve="Jean";
	document.getElementById("EleveChoisi").innerHTML="Voici Jean";
	document.getElementById("Image").src="Jean.jpg";
	Initialise();	
}

function Sylvie(){
	Eleve="Sylvie";
	document.getElementById("EleveChoisi").innerHTML="Voici Sylvie";
	document.getElementById("Image").src="Sylvie.jpg";
	Initialise();	
}
	
function Yan(){
	Eleve="Yan";
	document.getElementById("EleveChoisi").innerHTML="Voici Yan";
	document.getElementById("Image").src="Yan.jpg";
	Initialise();	
}


//////////////////////////////////////////
////////// CANVAS     ///////////////////
/////////////////////////////////////////

function miseajourcanvas() {
	var moncanvas=document.getElementById("Canvas"); 
	var ctx=moncanvas.getContext("2d");
	// Echelle
	var Ymax=0;
	for (var i=0;i<NbreIntervalle;i++){
		if (Ymax<Freq[i]) {Ymax=Freq[i]}
	}
	var Grady=25;
	var Unitey=Math.round(Ymax*100)/1000;
	var pas=40;
	var mult=1;
	if (NbreIntervalle==20){pas=20;grady=12;mult=0.5;}
	//   nettoyage
	moncanvas.height=350;
	moncanvas.width=550;
	ctx.offsetWidth=550;
	ctx.offsetHeight = 350;
	ctx.clearRect(0,0,moncanvas.width,moncanvas.height);
	ctx.globalAlpha = 1;         // opacit� max   
	ctx.lineWidth = 2;	  
	ctx.strokeStyle ="black";
	ctx.font = "10pt Comic sans Ms"
	// histogramme
	if (Ymax>0) {
		ctx.beginPath();
			ctx.moveTo(50,30);
			ctx.lineTo(50,300);
		ctx.closePath();   ctx.stroke();
		ctx.beginPath();
				ctx.moveTo(50,300);
				ctx.lineTo(450,300);
		ctx.closePath();   ctx.stroke();
		if (NbreIntervalle==10){ctx.fillText("Fr�quences x10",5,20);}
		if (NbreIntervalle==20){ctx.fillText("Fr�quences x20",5,20);}
		ctx.fillText("Intervalles",460,305);
		
		for (var i=1;i<NbreIntervalle+2;i++){
			ctx.lineWidth=1;
			if (i<NbreIntervalle) {
				ctx.beginPath();
				ctx.moveTo(50+pas*i,295);
				ctx.lineTo(50+pas*i,305);
				ctx.closePath();   ctx.stroke();
			}
			if (i<11) {
				ctx.beginPath();
				ctx.moveTo(45,300-Grady*i);
				ctx.lineTo(55,300-Grady*i);
				ctx.closePath();   ctx.stroke();
			}
			if (i<NbreIntervalle+1){
				ctx.fillStyle = "rgba(0,0,200,0.2)"; 
				ctx.fillRect(50+pas*(i-1),300-Math.round(Freq[i-1]*250/Ymax),pas,Math.round(Freq[i-1]*250/Ymax)); 
				ctx.strokeRect(50+pas*(i-1),300-Math.round(Freq[i-1]*250/Ymax),pas,Math.round(Freq[i-1]*250/Ymax)); 
			}
			if (i<12) {
				ctx.fillStyle = "black";
				ctx.fillText(Math.round(1/mult*(i-1)*Ymax*100)/100,10,305-Grady*(i-1)); 
				ctx.fillText((i-1)/10,45+40*(i-1),320); 	
			}
 			
		}
	}
	//courbes des densit�s 
		ctx.lineWidth = 4;	  
		ctx.strokeStyle ="rgba(255,0,0,0.5)";
		
		if ((Eleve=="Jean")&&(Total>9999)){
		ctx.beginPath();
			for (var i=1;i<100;i++) {
				ctx.moveTo(50+i*4,300-
					Math.round(mult*1/(2*Math.sqrt(i/100))*25/Ymax));
				ctx.lineTo(50+(i+1)*4,300-
					Math.round(mult*1/(2*Math.sqrt((i+1)/100))*25/Ymax));
			}
		ctx.closePath();   ctx.stroke();
		ctx.drawImage(Racine,452,300-mult*Math.round(1/(2*Math.sqrt(1))*25/Ymax)-15,60,30);		
		}
		
		
		
		if ((Eleve=="Sylvie")&&(Total>9999)){
		ctx.beginPath();
			for (var i=0;i<25;i++) {
				ctx.moveTo(50+i*16,300-
					Math.round(mult*1/(0.2*Math.sqrt(2*Math.PI))*Math.exp(-0.5*Math.pow((i/25-0.5)/0.2,2))*25/Ymax));
				ctx.lineTo(50+(i+1)*16,300-
					Math.round(mult*1/(0.2*Math.sqrt(2*Math.PI))*Math.exp(-0.5*Math.pow(((i+1)/25-0.5)/0.2,2))*25/Ymax));
			}
		ctx.closePath();   ctx.stroke();
		ctx.lineWidth = 1;	
		ctx.drawImage(Normal,370,80,150,60);			
		}
	
	if ((Eleve=="Yan")&&(Total>9999)){
		ctx.beginPath();
			ctx.moveTo(50,300-mult*Math.round(25/Ymax));
			ctx.lineTo(450,300-mult*Math.round(25/Ymax));
		ctx.closePath();   ctx.stroke();
		ctx.fillStyle ="black";
		ctx.fillText("f(x)=1",460,300-mult*Math.round(25/Ymax));			
		}
	
	

}



 
//////////////////////////////////////////
//////////////CORPS/////////////////////
////////////////////////////////////////



window.addEventListener("load", setupEvents); // attends le chargement complet pour activer les abonnements



