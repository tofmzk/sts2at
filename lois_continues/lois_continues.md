# Lois continues : Introduction



## Préambule : expérimentons



### Probabilités discrètes.

Lorsqu'on lance un dé à six faces non truqué, les résultats possibles de l'expérience aléatoire sont aisés à dénombrer : il y en a 6 qui son 1-2-3-4-5 et 6. Une telle expérience, où les événements sont dénombrables, est appelée expérience `discrète` . La loi d'équiprobabilité, très intuitive, nous dit que la probabilité d'obtenir 6, par exemple, et de une chance (cas favorable) sur 6 (nombre de possibilités totales), soit $`\frac 1 6`$.



### Choix d'un nombre réel dans [0;1].

On demande à trois élèves de choisir X un nombre au hasard dans l'intervalle [0;1]. `Au hasard` signifie qu'il ne doit pas être possible de prévoir à l'avance quel nombre il vont choisir. 

La différence avec le lancé de dé vu plus haut, c'est qu'entre 0 et 1, il y a une infinité de nombres et que nous, êtres humains, ne pouvons écrire tous ces nombres, ne serait-ce que parce qu'ils peuvent avoir une infinité de chiffres dans leur partie décimale. Les élèves décident donc de se limiter à 16 chiffres après la virgules , ce qui leur donne le choix entre $`10^{16} `$ nombres au lieu d'une infinité. Par rapport à l'infini, c'est peu. Cependant cela fait déjà beaucoup de nombres : $`10^{16} = 10 \times 10^6 \times 10^9`$ , soit dix millions de milliards de choix possibles.



On ne peut pas, comme c'est le cas lors d'une expérience discrète, noter chaque valeur possible car il y en a trop. 

La probabilité lorsqu'on prédise, à l'avance, le nombre choisi par un des élèves est de $`\frac 1 {10^{16}}= 0.0000000000000001`$ . Ca ne fait pas beaucoup... et elle serait de 0 s'il était possible de choisir X sans se limiter à 16 chiffres après la virgule.

 On ne va donc pas regarder la probabilité qu'à un nombre de sortir, mais celle qu'il tombe dans un intervalle.



Allons-y, simulons donc les tirages aléatoires de nos trois élèves en suivant [ce lien](simulateur/simulateur.html).



On remarque que :

* Chacun des élèves choisit bien aléatoirement car il est impossible de prédire le résultat de ce choix avec certitude.

    

* Si on n'effectue que quelques tirages, il est impossible de distinguer quoi que ce soit, tout semble très chaotique.

    

* En revanche, sur un grand nombre de tirages, on s'aperçoit que l'histogramme de la répartition des fréquences semble suivre une _tendance_ : il est possible de trouver une fonction $`f`$ dont la courbe épouse l'histogramme, et ce même en répétant, autant de fois qu'on le veut, les séries de tirages.

<img src="media/jean.jpg" alt="jean" style="zoom:50%;" />

<img src="media/sylvie.jpg" alt="Sylvie" style="zoom:50%;" />

<img src="media/yan.jpg" alt="Yan" style="zoom:50%;" />





* Chaque élève choisit au hasard un nombre dans [0;1], mais avec une certaine _stratégie_ :
    * Yan, choisit de façon `uniforme` : il y a autant de chance que le nombre _tombe_ dans chaque intervalle.
    * Jean, en réalité, procède comme Yan, puis met le résultat qu'il obtient au carré. En conséquence, les nombres sont plus souvent plus proches de 0 que de 1 car, $`si~x \in [0 ; 1]~alors~x² \leq x`$.
    * Sylvie a tendance a choisir des nombres autour de la moyenne 0.5 qu'aux extrémité 0 et 1. Nous verrons qu'il s'agit là d'une répartition `normale`.



### Du discret au continue



On souhaite maintenant essayer de prédire la probabilité que le prochain nombre a de tomber dans un intervalle. 

* On note que la probabilité pourrait être approchée par la fréquence de cet intervalle, lorsque le nombre d'expériences devient très très très très grand. Par exemple, dans le cas de Yan, la fréquence d'apparition d'un nombre dans l'intervalle [0,55; 0,60] serait de 0,051 environ (cf l'image juste au dessus, dernière ligne du tableau). 

* Cette fréquence, graphiquement, correspond à l'aire du rectangle de l'histogramme pour cet intervalle, peinte en bleu foncé ci-dessous. Le rectangle mesure 0.05 de large sur 1.02 (approximativement, regardez l'axe des ordonnées) de haut est à donc une aire de $`0,05 \times 1,02 = 0,051`$ : cela correspond bien à notre fréquence !

    <img src="media/yan_2.jpg" alt="yan_2" style="zoom:50%;" />



* Par exemple, $`p(0.2 \leq X \leq 0.5)`$ est égale à l'aire du rectangle bleu foncé ci-dessous, c'est à dire à la somme des aires des 6 rectangles de cet intervalle : $`0,049 + 0,051 + 0,050 + 0,048+ 0,050 + 0,050 \approx 0,3`$

    <img src="media/yan_3.jpg" alt="yan_3" style="zoom:50%;" />

    

* Connaissant la fonction $`f(x) = 1`$ qui _épouse_ notre histogramme(regardez sur le graphique ci-dessus), on pourrait _modéliser_ une loi de probabilité pour laquelle la probabilité qu'a X d'appartenir à un intervalle donné est l'aire située, dans cet intervalle, entre l'axe des abscisses et la courbe de $`f`$.  Par exemple, $`p(0.2 \leq X \leq 0.5) = 1 \times 0.3 = 0.3 `$ 

    



* Pour plus de précision, il faudrait diminuer la largeur des intervalles. Pour cela il faudrait en augmenter le nombre. Notre simulateur permet de prendre 10 intervalles (de largeur $`\frac 1 {10}`$) ou 20 intervalles (de largeur $`\frac 1 {20}`$). Il faudrait en prendre beaucoup plus : 100 (de largeur $`\frac 1 {100}`$), 1000 (de largeur $`\frac 1 {1000}`$), .... Dans l'idéal, le nombre d'intervalles devrait tendre vers $`\infty`$ et, alors, la largeur de nos rectangle tendrait, elle, vers 0.

    Pour calculer notre aire, il faudrait ajouter un nombre infini d'aires de rectangles. Chaque rectangle, s'il est situé à l'abscisse $`x`$, aurait pour hauteur $`f(x)`$, une largeur infinitésimale notée $`dx`$ et donc une aire $`f(x) dx`$. Nous passons alors d'une somme `discrète` à une somme continue, qu'on appelle `intégrale`. 

    On note ainsi $`\int_{a}^{b}{f(x)dx}`$, c'est à dire `intégrale` (somme continue) de $`f(x) dx`$, pour $`x`$ allant de _a_ à _b_, l'aire en question.

    Nous sommes passé d'une somme discrète à une somme continue.



## Loi continue



### Définition

On dit qu'une variable aléatoire X, qui prend ses valeurs dans un intervalle $`I`$ de $`\R`$ , suit une loi continue, s'il existe une fonction $`f`$, appelée densité de la loi, telle que :

* $`f`$ est définie et continue sur $`I`$.
* pour tout $`x \in I, f(x) \ge 0`$
* sur$`I`$, l'aire totale comprise entre la courbe de $`f`$ est l'axe des abscisses vaut 1.
* Pour tout $`a`$ et$`b`$ de $`I`$, avec $`a \le b`$, $`p(a \ge X \ge b) = \int_{a}^{b} {f(x) dx}`$



_Exemple_ :

Si nous reprenons le cas de Yan, la variable aléatoire $`X \in [0; 1]`$, correspondant au nombre qu'il choisit , suit une loi continue de densité $`f(x) = 1`$ sur $`I=[0; 1]`$.



_Remarque :_

si X suit une loi continue de densité $`f`$  sur $`I`$, alors, pour tout $`a \in I`$ ,$` p(X = a) = p(a \le X \le a) =\int_{a}^{a} {f(x) dx} = 0`$

Autrement dit, entre les abscisses $`a`$ et $`a`$, il n'y a pas d'aire.



## Loi uniforme



Nous l'avons évoqué plus haut, le cas de Yan est un cas `uniforme`. Dans ce cas, la densité est constante.

Pour la trouver, il suffit de se servir du fait que l'aire totale entre l'axe des abscisses et la courbe, sur l'intervalle, doit valoir 1.

Dans le cas de Yan, puisque l'intervalle [0; 1] à pour largeur 1, la hauteur du rectangle doit également être de 1 de façon à ce que l'aie total fasse $`1 \times 1 = 1`$.



### Définition

On dit qu'une variable aléatoire X, qui prend ses valeurs dans un intervalle [m, n] de $`\R`$ ,  (avec m < n), suit une loi uniforme,  si elle admet pour densité sur cet intervalle $`f(x)=\frac 1 {n - m}`$. Cette loi uniforme est notée U(m, n).



![uniforme](media/uniforme.jpg)



_Exemple :_ 

Soit X une variable aléatoire de loi U(1, 5).

* Quelle sera la densité de X ?
* calculez :
    * $`p(1 \le X \le 2)`$
    * $`p(X \ge 1)`$
    * $`p(X = 1)`$

### Exercices



Vous trouverez [quelques exercices en cliquant ici](exercices.pdf)



### Espérance et écart-type

Soit X une variable aléatoire suit une loi uniforme de densité $`f`$ sur un intervalle [m; n] (avec m < n).

Alors :

* L'espérance de X est $`E(X) = \frac {m+n} 2`$
* L'écart-type de X est $`\sigma (X) = \frac {n - m} {2 \sqrt 3}`$



## Loi exponentielle

### Introduction

Allons-y pour [l'exercice 1 de cette fiche](loi_expo_exos.pdf) !



### Définition et premières propriétés



On dit qu'une variable aléatoire X à valeurs dans$`[0; + \infty[`$ suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ si et seulement si, pour tout $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$ avec _a < b_ ,on a : $`p(a \leq X \leq b) = \int_a^b{\lambda e^{-\lambda x} dx}`$



**Remarques:**

Quelque soit  $`\lambda \in [0; + \infty[`$ , une primitive de $`f(x) = \lambda e^{-\lambda x}`$ est $`F(x) = -e^{-\lambda x}`$.

Ainsi :

 $`p(a \leq X \leq b) = \int_a^b{\lambda e^{-\lambda x} dx} = F(b)-F(a) = -e^{-\lambda b}--e^{-\lambda a} = e^{-\lambda a} - e^{-\lambda b}`$

On a également le cas particulier où $`a = 0`$ qui nous donne :

$`p(X \leq b) = e^0 - e^{-\lambda b} = 1 - e^{-\lambda b}`$



**Propriétés :**

Si une variable aléatoire X à valeurs dans$`[0; + \infty[`$ suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ , alors, pour tout $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$ avec _a<b_, on a :

*  $`p(a \leq X \leq b) =  e^{-\lambda a} + e^{-\lambda b}`$
* $`p(X \leq b) =  1 - e^{-\lambda b}`$



### Exercices

Allez, faisons les [Exercices 2-3 de cette fiche](loi_expo_exos.pdf) 



### Loi de durée de vie sans vieillissement.

La loi exponentielle est utilisée pour modéliser la durée de vie d'éléments particuliers... Voyons lesquels.

Soit une variable aléatoire X à valeurs dans$`[0; + \infty[`$ qui suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ . Soit $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$  avec _a<b_.

$`p_{X \ge a}(X \leq b) = \frac {p(a \leq X \leq b)} {p(X \ge a)}= \frac {e^{-\lambda a} - e^{-\lambda b}} {1 - p(X \le a)} = \frac {e^{-\lambda} \times (e^a - e^b)}{1 - 1 + e^{-\lambda a}} = \frac {e^a - e^b}{e^a} = 1- e^{b - a}`$



Si on pose $`b = a+ t`$ on obtient :

$`p_{X \ge a}(X \le a + t) = 1-e^{a+t-a} = 1-e^t `$



Cela signifie que cette probabilité ne dépend pas de _a_ mais uniquement de _t_.

Ainsi, si X représente la durée de vie d'un élément, la probabilité qu'il vive durant un laps de temps _t_ ne dépend pas de l'âge _a_ qu'a cet élément mais seulement du laps de temps _t_. 

Les êtres humains, le animaux et toute chose _vivante_ voit sa probabilité de mourir augmenter au fur et à mesure qu'ils deviennent vieux.

Pour notre élément, ce n'est  pas le cas : il peut _mourir_ durant un laps de temps mais cela ne dépend pas de son âge au début de ce laps de temps : il s'agit d'un élément qui ne vieillit pas !



Voilà pourquoi, pour la loi exponentielle,on parle de loi de durée de vie sans vieillissement.

Quelques exemples :

* La durée de vie d'un composant électronique : la probabilité qu'un appareil électronique tombe en panne dans un laps de temps donné ne dépend pas de l'âge du composant.

* La durée de vie d'un atome radioactif : un atome qui est radioactif depuis longtemps n'a pas plus de chance de se désintégrer dans la minute qui vient qu'un atome radioactif qui vient d'être créé.

* La durée de vie d'un verre en cristal. En effet,  la probabilité pour le verre d'être cassé par exemple dans les cinq ans ne dépend pas de sa date de fabrication.

* Le temps écoulé entre deux accidents de voiture dans lequel un individu donné est impliqué: si pendant 2 ans vous n'avez pas eu d'accident, cela n'augmente pas la probabilité que vous en ayez un dans l'année qui vient

    

### Exercices

Allez, faisons les [Exercices 4-5 de cette fiche](loi_expo_exos.pdf) 



### Espérance et écart-type



**Propriété** :

Soit une variable aléatoire X à valeurs dans$`[0; + \infty[`$ qui suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ . 

Alors :

* $`E(X)= \frac 1 \lambda`$
* $`\sigma(X)= \frac 1 \lambda`$



**Conséquence :**

Plus $`\lambda`$ est grand plus l'espérance de vie est faible et inversement.

Pour un atome d' Uranium 234, $`\lambda \approx 2.56721 \times 10^{-9}`$ ....



## Loi Normale



### Définitions



On dit qu'une variable aléatoire X à valeurs dans $`\R`$ suit une loi normale de paramètre $`\mu`$ et $`\sigma`$ si et seulement si sa densité de probabilité est  

$`f(x)= \frac {1} {\sigma \sqrt {2 \pi }} e^{-\frac 1 2(\frac {x - m} {\sigma})^2}`$. 

Alors :

* on note $`X \rightarrow N(\mu, \sigma )`$
* $`E(X) = \mu`$ et $`\sigma(X) = \sigma`$.

* La courbe représentative de la densité est appelée `courbe de Gauss` ou `courbe en cloche`. Elle possède un axe de symétrie d'équation $`x = \mu`$.

![courbe de Gauss](media/normale1.jpg)

* Si l'écart-type augmente, la courbe _s'élargit_ :

    ![courbe de Gauss](media/normale2.jpg)

* Si la moyenne augmente, la courbe se décale vers la droite.

![courbe de Gauss](media/normale4.jpg)

* Si la moyenne diminue, la courbe se décale vers la gauche.

![courbe de Gauss](media/normale_3.jpg)

### Calculer avec la loi normale

Si $`X \rightarrow N(\mu, \sigma)`$ alors $`p(a \leq X \leq b) ={\frac {1} {\sigma \sqrt {2 \pi }} \int_a^b  e^{-\frac 1 2(\frac {x - m} {\sigma})^2}}`$.

On ne peut calculer cette intégrale en valeur exacte : nous allons devoir utiliser la calculatrice et donner des résultats en valeurs approchées.

C'est parti pour les exercices 1, 2, 3 et 4 de [cette fiche](loi_normale_exos.pdf).

### Loi normale et loi binomiale



Soit $`X \rightarrow B(n, p)`$.

Si le conditions suivantes sont toutes réunies :

* $`n \geq 30`$
* $`np \geq 5`$
* $`n(1-p) \geq 5`$

Alors $`X`$ loi de $`X`$ peut être approximée par la loi normale $`N(E(X), \sigma(X))`$ avec :

* $`E(X) = np`$
* $`\sigma(X) = \sqrt {np(1-p)}`$

_Remarque : Cela découle du théorème de `Moivre-Laplace` qui date du début du 19ème siècle_.



Allez, on retourne voire [cette fiche](loi_normale_exos.pdf) et l'exercice 5.

### Loi normale centrée réduite



Lorsque $`X \rightarrow N(0,1)`$ , on dit que X suit la loi `normale centrée`. On a alors :

* $`f(x)=\frac 1 {2 \pi}e^{-\frac 1 2 x²}`$
*  $`p(a \leq X \leq b) ={\frac {1} {\sqrt {2 \pi }} \int_a^b  e^{-\frac 1 2 x²}}`$.

​    

![courbe de Gauss centrée](media/normale_centree.jpg)

**Propriété : changement de variable**

Si $`X \rightarrow N(\mu, \sigma)`$ alors la variable $`T = \frac {X - \mu} {\sigma}`$ suit la loi normale centrée réduite.

Ainsi, on peut ramener toute loi normale à la loi centrée réduite par changement de variable.

**Conséquence (démontrée en exercice)**:

Si $`X \rightarrow N(\mu, \sigma)`$  alors :

* $`p(\mu - \sigma \leq X \leq \mu + \sigma ) \approx 0.68`$
* $`p(\mu - 2\sigma \leq X \leq \mu + 2\sigma ) \approx 095`$
* $`p(\mu - 3\sigma \leq X \leq \mu + 3\sigma ) \approx 0.997`$

Ces valeurs particulières sont importantes et doivent être connues. Nous en reparlerons souvent.





Allons le voir dans les exercices suivants de [notre fiche](loi_normale_exos.pdf).



___

Mieszczak Christophe CC BY SA