# Loi exponentielle

### Introduction

Allons-y pour [l'exercice 1 de cette fiche](loi_expo_exos.pdf) !



### Définition et premières propriétés



On dit qu'une variable aléatoire X à valeurs dans$`[0; + \infty[`$ suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ si et seulement si, pour tout $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$ avec _a < b_ ,on a : $`p(a \leq X \leq b) = \int_a^b{\lambda e^{-\lambda x} dx}`$



**Remarques:**

Quelque soit  $`\lambda \in [0; + \infty[`$ , une primitive de $`f(x) = \lambda e^{-\lambda x}`$ est $`F(x) = -e^{-\lambda x}`$.

Ainsi :

 $`p(a \leq X \leq b) = \int_a^b{\lambda e^{-\lambda x} dx} = F(b)-F(a) = -e^{-\lambda b}--e^{-\lambda a} = e^{-\lambda a} - e^{-\lambda b}`$

On a également le cas particulier où $`a = 0`$ qui nous donne :

$`p(X \leq b) = e^0 - e^{-\lambda b} = 1 - e^{-\lambda b}`$



**Propriétés :**

Si une variable aléatoire X à valeurs dans$`[0; + \infty[`$ suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ , alors, pour tout $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$ avec _a<b_, on a :

*  $`p(a \leq X \leq b) =  e^{-\lambda a} + e^{-\lambda b}`$
*  $`p(X \leq b) =  1 - e^{-\lambda b}`$



### Exercices

Allez, faisons les [Exercices 2-3 de cette fiche](loi_expo_exos.pdf) 



### Loi de durée de vie sans vieillissement.

La loi exponentielle est utilisée pour modéliser la durée de vie d'éléments particuliers... Voyons lesquels.

Soit une variable aléatoire X à valeurs dans$`[0; + \infty[`$ qui suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ . Soit $`a \in [0; + \infty[`$ et $`b \in [0; + \infty[`$  avec _a<b_.

$`p_{X \ge a}(X \leq b) = \frac {p(a \leq X \leq b)} {p(X \ge a)}= \frac {e^{-\lambda a} - e^{-\lambda b}} {1 - p(X \le a)} = \frac {e^{-\lambda} \times (e^a - e^b)}{1 - 1 + e^{-\lambda a}} = \frac {e^a - e^b}{e^a} = 1- e^{b - a}`$



Si on pose $`b = a+ t`$ on obtient :

$`p_{X \ge a}(X \le a + t) = 1-e^{a+t-a} = 1-e^t `$



Cela signifie que cette probabilité ne dépend pas de _a_ mais uniquement de _t_.

Ainsi, si X représente la durée de vie d'un élément, la probabilité qu'il vive durant un laps de temps _t_ ne dépend pas de l'âge _a_ qu'a cet élément mais seulement du laps de temps _t_. 

Les êtres humains, le animaux et toute chose _vivante_ voit sa probabilité de mourir augmenter au fur et à mesure qu'ils deviennent vieux.

Pour notre élément, ce n'est  pas le cas : il peut _mourir_ durant un laps de temps mais cela ne dépend pas de son âge au début de ce laps de temps : il s'agit d'un élément qui ne vieillit pas !



Voilà pourquoi, pour la loi exponentielle,on parle de loi de durée de vie sans vieillissement.

Quelques exemples :

* La durée de vie d'un composant électronique : la probabilité qu'un appareil électronique tombe en panne dans un laps de temps donné ne dépend pas de l'âge du composant.

* La durée de vie d'un atome radioactif : un atome qui est radioactif depuis longtemps n'a pas plus de chance de se désintégrer dans la minute qui vient qu'un atome radioactif qui vient d'être créé.

* La durée de vie d'un verre en cristal. En effet,  la probabilité pour le verre d'être cassé par exemple dans les cinq ans ne dépend pas de sa date de fabrication.

* Le temps écoulé entre deux accidents de voiture dans lequel un individu donné est impliqué: si pendant 2 ans vous n'avez pas eu d'accident, cela n'augmente pas la probabilité que vous en ayez un dans l'année qui vient

      

### Exercices

Allez, faisons les [Exercices 4-5 de cette fiche](loi_expo_exos.pdf) 



### Espérance et écart-type



**Propriété** :

Soit une variable aléatoire X à valeurs dans$`[0; + \infty[`$ qui suit une loi exponentiel de paramètre $`\lambda \in [0; + \infty[`$ . 

Alors :

* $`E(X)= \frac 1 \lambda`$
* $`\sigma(X)= \frac 1 \lambda`$



**Conséquence :**

Plus $`\lambda`$ est grand plus l'espérance de vie est faible et inversement.

Pour un atome d' Uranium 234, $`\lambda \approx 2.56721 \times 10^{-9}`$ ....