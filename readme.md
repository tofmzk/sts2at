# Fil

Voici le fil du déroulement du cours pendant la période de confinement.

Le travail effectué est noté chaque heure sur Pronote.

Les documents sont au format :

* _pdf_ qui se lit avec, par exemple ,[Acrobat reader](https://get2.adobe.com/fr/reader/otherversions/) qu'il vous faudra installer si vous ne l'avez pas déjà fait.

* _md_, le markdown, qui se lit avec, par exemple, [Typora](https://typora.io/#windows) qu'il vous faudra installer si vous ne l'avez pas déjà fait.

    

## BTS BLANC

Voici le sujet du [BTS BLANC du 7 avril](devoirs/sujet1.pdf) :

* Vous répondez sur copie, comme d'habitude.
* Vous avez jusqu'à 11h30 soit 1h30 de plus que la normale. Passé ce délais, il y aura des pénalités.
* A l'issue de l'épreuve :
    * vous photographiez chaque page en prenant soin de tout mettre dans le même sens.
    * vous renommez les photos `page1`, `page2` ...
    * vous me renvoyez vos pages via l'ENT.
* Je serai présent en soutient sur jitsi meet : [https://meet.jit.si/RudeThreadsSpillGradually](https://meet.jit.si/RudeThreadsSpillGradually)

BON COURAGE !!



## Sujets de type BTS

*  [BTS BLANC du 7 avril](devoirs/sujet1.pdf) 
    *  [Correction](devoirs/correction_bts_blanc.md)
* [Sujet no2](devoirs/sujet_2.pdf)
    * [Correction](devoirs/corrige_2.md)
* [Sujet no3](devoirs/sujet_3.pdf)
    * [Correction](devoirs/corrige_3.md)
*  [Sujet no 4](devoirs/sujet_4.pdf)
    * [Correction](devoirs/corrige_4.md)


## Exercices

* [DM : Le fil qui chauffe](devoirs/le_fil_qui_chauffe.pdf) : fonction exponentielle, développement limité en 0.
* [Loi continues](lois_continues/exercices.pdf)
* [Intégration](integration/exos.pdf)
* [Loi exponentielle - exercices](lois_continues/loi_expo_exos.pdf)
* [interro intégration et loi exponentielle](devoirs/integration_loi_expo.md) $`\rightarrow`$ [correction](devoirs/correction_integration_expo.md)
* [Equations différentielles](equa_diff/exos.pdf)
* [Loi normale](lois_continues/loi_normale_exos.pdf)
* [intervalles de confiance](stat_inferentielles/intervalles_de_confiance.pdf)
* [test d'hypothèse](test_hypothese/test_hypothese.md)



## Résumés de cours

* [Lois continues : introduction](lois_continues/introduction.md)

* [Loi uniforme](lois_continues/loi_uniforme.md)

* [intégration](integration/calcul_integral.md)

* [Loi exponentielle](lois_continues/loi_expo.md)

* [Equations différentielles d'ordre 1](equa_diff/ordre_1.md).

* [Loi normale](lois_continues/loi_normale.md)

* [Equations différentielles d'ordre 2](equa_diff/ordre_2.md).

* [statistiques inférentielles, intervalles de confiance](stat_inferentielles/stat_inferentielles.md)

    



____________

Par Mieszczak Christophe

Licence CC BY SA