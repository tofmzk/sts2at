# Équations différentielles d'ordre 2

### Définitions



> Une équation différentielle d'ordre 2 sur un intervalle $`I \subset \R`$ est une équation de la forme $`(E):ay''+by' +cy = d`$ où :
>
> * $`y`$ est une fonction inconnue définie, continues et dérivable sur $`I`$.
> * $`y'`$ est la dérivée de $`y`$ et $`y''`$ celle de $`y'`$.
> * $`a,~b,~c`$ sont trois réels (_a_ est non nul)
> * _d_ est une fonction continue et dérivable sur _I_.



_Exemple_:

$`(E) : 2y'' - 5y' + 3y = 3x² -13x+9`$

* _a_= 2
* _b_ = -5
* _c_ = 3
* $`d(x) = 3x² -13x+9`$

> Toute équation différentielle d'ordre 2$`(E) : ay''+by' +cy=d`$ admet pour équation réduite l'équation différentielle d'ordre 2 $`(E_0) : ay''+by' + cy=0`$.



_Exemple :_ 

$`(E) : 2y'' - 5y' + 3y =3x² -13x+9`$ admet pour équation réduite $`(E_0) : 2y'' - 5y' + 3y = 0`$



Pour résoudre une telle équation il nous faudra :

* Résoudre son équation réduite.
* Trouver une solution particulière de _(E)_.
* Ajouter les deux solutions précédentes pour déterminer la forme générale des solutions de _(E)_
* Pour finir, regarder les conditions initiales de l'équation pour déterminer l'unique solution de_(E)_ qui leur convient.

### Résolution de $`(E_0)`$

On applique le protocole suivant :

* On note l'équation caractéristique suivante : $`ar²+br+c = 0`$
* On calcule le discriminant $`\Delta = b²-4ac`$ de cette équation.
    * si $`\Delta = 0`$ alors elle a une unique solution $`r_1 = -\frac b {2a}`$ et la forme générale des solutions de $`(E_0)`$ est $`(\lambda x + \mu)e^{r_1 x}`$ où $`\lambda`$ et $`\mu`$ sont des paramètres réels dépendants des conditions initiales. 
    * si $`\Delta > 0`$ alors elle a deux solutions $`r_1 = \frac {-b + \sqrt {\Delta}} {2a}`$ et $`r_2 = \frac {-b - \sqrt {\Delta}} {2a}`$. Dans ce cas, la forme générale des solutions de $`(E_0)`$ est $`\lambda  e^{r_1 x}+ \mu e^{r_2 x}`$ où $`\lambda`$ et $`\mu`$ sont des paramètres réels dépendants des conditions initiales. 
    * le cas  $`\Delta < 0`$ n'est plus étudié en BTS ATI. Il entraîne des solutions où interviennent des fonctions circulaires (cosinus et sinus) et une exponentielle. On les trouve, par exemple, lors de phénomènes d'oscillations amorties (oscillation d'un ressort ... )

_Exemple_:

$`(E) : 2y'' - 5y' + 3y = 3x² -13x+9`$ admet pour équation réduite $`(E_0) : 2y'' - 5y' + 3y = 0`$

* _a_= 2
* _b_ = -5
* _c_ = 3
* L'équation caractéristique est $`2r²+-5r+3 = 0`$
    * $`\Delta = (-5)² - 4 \times 2 \times 3 = 1`$
    * les solutions sont : 
        * $`r_1 = \frac {-b + \sqrt {\Delta}} {2a} = \frac {5 + 1}{4} = \frac 3 2`$
        *  $`r_2 = \frac {-b - \sqrt {\Delta}} {2a} = \frac {5 - 1}{4} = 1`$
    * La forme générale des solutions de $`(E_0)`$ est  $`\lambda  e^{\frac 3 2 x}+ \mu e^{ x}`$ où $`\lambda`$ et $`\mu`$ sont des paramètres réels dépendants des conditions initiales. 

### Trouvons une solution de $`(E)`$

> Il nous faut maintenant trouver une solution particulière de (E). N'importe laquelle. Pas besoin de les avoir toute, une solution suffit.
>
> En BTS, l'énoncé fournit une indication majeure qui permet de trouver cette solution particulière. En suivant cette indication, il est souvent aisé de la trouver.

_Exemple :_ 

Montrer que $`(E) : 2y'' - 5y' + 3y = 3x² -13x+9`$ admet pour solution particulière $`h(x) = x² -x`$

On remplace l'inconnue $`y`$ par $`h`$ dans le membre de gauche puis on calcule en espérant arriver au membre de droite pour prouver que $`h`$ est bien une solution particulière de _(E)_.

$`2h'' - 5h' + 3h \\ = 2(x²-x)'' - 5 (x²-x)' +3 (x²-x)\\ = 2 (2x-1)' - 5(2x-1) + 3x²-3x \\= 2 \times 2 - 10x +5 + 3x²-3x \\= 3x² -13x+9`$

$`h(x) = x² -x`$ est donc bien une solution particulière de _(E)_.

### Trouver toutes les solutions de (E)



> Nous disposons :
>
> * de toutes les solutions de $`(E_0)`$  .
> * d'une solution particulière de $`(E)`$ 
>
> La forme générale des solutions de  $`(E)`$ s'obtient simplement en ajoutant les deux solutions précédentes.
>
> 



_Exemple :_

En reprenant notre équation différentielle,la forme des solutions de _(E)_ est  :

 $`x² -x + \lambda  e^{\frac 3 2 x}+ \mu e^{ x}`$ où $`\lambda`$ et $`\mu`$ sont des paramètres réels dépendants des conditions initiales. 

### Conditions initiales



>  Parmi toutes les solutions possibles de l'équation nous allons en chercher une dont on connaît une valeur particulière : ce sont les conditions initiales. 
>
>  Une fois ces conditions posées, il ne reste plus qu"une unique fonction répondant au problème.

_Remarque :_

le principe est exactement le même que pour les équations différentielles d'ordre 1. Cependant, puisque nous avons ici deux paramètres à déterminer, il nous faut deux équations et donc deux indications sur les conditions initiales.



_Exemple :_ 

Chercher l'unique solution $`f`$ de _(E)_ telle $`f(0)= 1`$ et $`f'(0) = 0`$.

On sait que  $`f(x) = x² -x + \lambda  e^{\frac 3 2 x}+ \mu e^{ x}`$ 

* $`f(0) = 1`$ donc, en remplaçant $`x`$ par 0, il vient : $`\lambda + \mu = 1`$

* $`f'(x) = 2x - 1 + \frac 3 2 \lambda e^{\frac 3 2 x} + \mu e^x$.

    * $`f'(0) = 0`$ donc, en remplaçant $`x`$ par 0, il vient : $`\frac 3 2 \lambda + \mu -1 = 0`$ soit $`\frac 3 2 \lambda + \mu  = 1`$
    * il nous faut donc maintenant résoudre le système d'équation suivant :

    $` \left\{ \begin{array}{ll}     \lambda + \mu = 1 \\      \frac 3 2 \lambda + \mu =1 \end{array}\right.`$

    $` \left\{ \begin{array}{ll}   - \frac 1 2 \lambda  = 0 \\      \frac 3 2 \lambda + \mu =1 \end{array}\right.`$ en soustrayant la ligne 1 et la ligne 2

    $` \left\{ \begin{array}{ll}     \lambda =0 \\      \frac 3 2 \times -0+ \mu =1 \end{array}\right.`$ On trouve alors la première inconnue et on remplace dans la deuxième ligne

    $` \left\{ \begin{array}{ll}     \lambda = 0 \\      \mu = 1\end{array}\right.`$ 

    

    Ainsi,l'unique solution convenant à ces conditions initiales est $`f(x) = x² -x  + e^{ x}`$ 

    

    _Remarque :_

    D'une apparence qui peut sembler compliquée, les systèmes obtenues se résoudront le plus souvent de façon très simple. N'ayez pas peur et lancez vous !

    

    Allons-y : [exercices](exos.pdf)

    ________

    Par Mieszczak Christophe

    Licence CC BY SA

    

    