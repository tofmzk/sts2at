# Résoudre une équation différentielle



## Ordre 1

### Définitions



> Une équation différentielle d'ordre 1 sur un intervalle $`I \subset \R`$ est une équation de la forme $`(E):ay'+by = c`$ où :
>
> * $`y`$ est une fonction inconnue définie, continues et dérivable sur $`I`$.
> * $`a,~b,~c`$ sont trois fonctions définies et continues sur $`I`$. 

_Exemple :_

$`(E) : y'-xy=-2x²-x+2`$ est une équation différentielle sur $`\R`$ d'ordre 1 où :

* $`a(x) = 1`$
* $`b(x) = -x`$
* $`c(x) = -2x²-x+2`$



> Toute équation différentielle d'ordre 1$`(E) : ay'+by = c`$ admet pour équation réduite l'équation différentielle d'ordre 1 $`(E_0) : ay'+by=0`$.



_Exemple :_ 

$`(E) : y'-xy=-2x²-x+2`$ admet pour équation réduite $`(E_0) : y'-xy=0`$



Pour résoudre une telle équation il nous faudra :

* Résoudre son équation réduite.
* Trouver une solution particulière de _(E)_.
* Ajouter les deux solutions précédentes pour déterminer la forme générale des solutions de _(E)_
* Pour finir, regarder les conditions initiales de l'équation pour déterminer l'unique solution de_(E)_ qui leur convient.

### Résolution de $`(E_0)`$



> On va commencer par résoudre l'équation réduite. Pour cela, il y a une méthode simple en 3 étapes :
>
> * On pose $`g(x) = \frac {b(x)} {a(x)}`$
> * On détermine une primitive $`G`$ de $`g`$ c'est à dire telle que $`G' = g`$.
> * L'ensemble des solutions de $`(E_0)`$ s'écrivent sous la forme $`Ke^{-G(x)}`$ où $`K \in \R`$

_Remarque :_

Une telle équation n'a pas une unique solution mais une infinité, la constante $`K`$ pouvant prendre n'importe quelle valeur.

Vérifions en remplaçant la solution dans le membre de gauche de notre équation réduite. Cela ne prouvera pas que toutes les solutions sont de cette forme mais que toutes les fonctions de cette forme sont solution. On se contentera de cela pour le moment.



$`a \times (Ke^{-G(x)})' + b \times Ke^{-G(x)} `$

$`= a \times -G'(x) \times Ke^{-G(x)} + b \times Ke^{-G(x)}`$

$` = a \times -g \times Ke^{-G(x)} + b \times Ke^{-G(x)}`$

$`=a \times - \frac b a \times Ke^{-G(x)}+ b \times Ke^{-G(x)} `$

$`=-b \times Ke^{-G(x)}+ b \times Ke^{-G(x)} `$

 $`= 0`$

Toute fonction de la forme $`Ke^{-G(x)}`$ où $`K \in \R`$ et  donc bien solution de $`(E_0)`$



_Exemple :_

 $`(E_0) : y'-xy=0`$

* $`g(x) = \frac {-x}{1} = -x`$
* Une primitive de g a pour expressions $`G(x) = - \frac 1 2 x²`$
* L'ensemble des solutions de $`(E_0)`$ s'écrivent sous la forme $`Ke^{-G(x)}=Ke^{\frac 1 2 x²}`$ où $`K \in \R`$



### Trouvons une solution de $`(E)`$

> Il nous faut maintenant trouver une solution particulière de (E). N'importe laquelle. Pas besoin de les avoir toute, une solution suffit.
>
> En BTS, l'énoncé fournit une indication majeure qui permet de trouver cette solution particulière. En suivant cette indication, il est souvent aisé de la trouver.



_Exemple :_

(E) : $`y'-xy=-2x²-x+2`$

_indication_ : Montrez que la fonction $`h(x) = 2x+1`$ est une solution de (E).

Comme pour toute équation, il suffit de remplacer l'inconnue, c'est à dire $`y`$ par la fonction proposée, c'est à dire $`h`$ dans le membre de gauche et de voir, en calculant de proche en proche, si on finit par arriver au membre de droite.



$`h' - xh = (2x+1)' -x(2x+1) = 2 -2x²-x = -2x²-x+2`$ : on est bien arrivé au second membre

Nous venons de trouver une solution particulière de(E) : $`h(x) = 2x+1`$.



### Trouver toutes les solutions de (E)



> Nous disposons :
>
> * de toutes les solutions de $`(E_0)`$  qui sont de la forme $`Ke^{-G(x)}`$ où $`K \in \R`$
> * d'une solution particulière de $`(E)`$ 
>
> La forme générale des solutions de  $`(E)`$ s'obtient simplement en ajoutant les deux solutions précédentes.
>
> 



_Exemple :_

Reprenons (E) : $`y'-xy=-2x²-x+2`$ :

* L'ensemble des solutions de $`(E_0)`$ s'écrivent sous la forme $`Ke^{-G(x)}=Ke^{\frac 1 2 x²}`$ où $`K \in \R`$
*  $`h(x) = 2x+1`$ est une solution particulière de (E).



Ainsi la forme générale des solutions de (E) est :$`2x+1+ Ke^{\frac 1 2 x²}`$ où $`K \in \R`$



_Remarque :_

Encore une fois, une telle équation n'a pas une unique solution mais une infinité, la constante $`K`$ pouvant prendre n'importe quelle valeur.



### Condition initiale



>  Parmi toutes les solutions possibles de l'équation nous allons en chercher une dont on connaît une valeur particulière : ce sont les conditions initiales. 
>
> Une fois ces conditions posées, il ne reste plus qu"une unique fonction répondant au problème.



_Exemple :_

Cherchons l'unique solution $`f`$ de (E) telle que $`f(0) = 2`$.

* on sait qu'il existe $`K \in \R`$ tel que $`f(x)=2x+1+ Ke^{\frac 1 2 x²}`$  : reste à trouver la valeur de $`K`$ qui convient.

* on sait que $`f(0) = 2`$ donc, en remplaçant $`x`$ par 0 on trouve :

    $`2 \times 0 + 1 + K e^{\frac 1 2 \times 0} =2`$

    $`1+Ke^0 = 2`$

$`1+K=2`$

On trouve donc $`K=1`$ !



En remplaçant $`K`$ par 1, l'unique solution de (E) qui vaut 2 en 0 est donc $`f(x)=2x+1+ e^{\frac 1 2 x²}`$ 





_________

Par Mieszczak Christophe licence CC BY SA