# Corrigé DM2



### Algèbre



**Partie A :**



1. $`E : y ' + 2,5 y = 0`$

	**Méthode Générale**

	$`a =1, b= 2.5`$

	* $`g(x)= \frac b a = \frac {2.5} {1} = 2.5`$

	* une primitive de g est G(x) = $`2.5 x`$

		La forme générale des solution de $`E_0`$ est $`K e^{-G(x)} = K e^{-2.x}`$ où $` K \in \R`$

	

	**Méthode de l'exo**

	$`E : y' + 2.5y = 0`$ 

	$` a = 2.5`$

	La forme générale des solutions de $`E_0`$ est $`K e^{-ax}=K e^{-2.5x}`$ où $` K \in \R`$.

2. $`S(x) = K e^{-2.5x}`$ et $`S(0) = 2`$

	

	$`K e^0 = 2`$

	$`K = 2`$

	Donc $`S(x) = 2e^{-2.5x}`$ est la solution recherchée.

	

	**Partie B**

	1. a.	$`\lim\limits_{x \to +\infty} e^{-2.5x} = 0`$ 

		b.  $`f(x) - y = x + e^{-2.5x} - x = e^{-2.5x}`$

		or $`\lim\limits_{x \to +\infty} e^{-2.5x} = 0`$ donc $`\Delta : y = x`$ est asymptote à $`C`$ en $`+  \infty`$.

		c. Comme une fonction exponentielle est toujours strictement positive donc $`f(x) - y >0`$ donc $`C`$ est au dessus de $`\Delta`$.

		

	2. a. $`f'(x) = (x + e^{-2.5 x})'`$ 

		$`f'(x) = 1 + (-2.5) \times e^{-2.5x}`$

		$`f'(x)= 1 -2.5 e^{-2.5x}`$

		

		b. $`1-2.5e^{-2.5x}=0`$

		$` 1 = 2.5 e^{-2.5x}`$

		$`\frac 1 {2.5} = e^{-2.5x}`$

		$`ln(0.4) = ln(e^{-2.5x})`$

		$`ln(0.4) = -2.5x`$

		$`x = \frac {ln(0.4)}{-2.5}`$

		$`x \approx 0.37`$

		

		c.

		![variations](variations_dm2.jpg)

		

		3. Le $`DL_3(0)`$ de $`f`$ est $`f(x)=  2 - 4x + \frac {25}  4 \times x² + x² \epsilon(x)`$ avec $`\lim\limits_{x \to 0} \epsilon (x) = 0`$ 

			$`T_0 : y = 2-4x = -4x + 2`$

			Comme $`\frac {25} 4 \times x²`$ est toujours positif donc $`C`$ est au dessus de $`T_0`$

		

		**Partie C**

		1. 

		![algo](dm2_algo1.jpg)

		

		L'algo considère que l'aire sous la courbe entre 1 et 2 est sensiblement la même que la somme des aires des 5 rectangles.

		

		| k                                     | 0          | 1         | 2         | 3         | 4         |
		| ------------------------------------- | ---------- | --------- | --------- | --------- | --------- |
		| $`\frac 1 5 \times f(1 + \frac k 5)`$ | $` 0.233`$ | $`0.260`$ | $`0.292`$ | $`0.327`$ | $`0.364`$ |
		| $`A_{totale}`$                        | 0.233      | 0.493     | 0.785     | 1.112     | 1.476     |

		

	2. D'après le logiciel, une primitive de $`f`$ est $`F(x)= \frac 1 2 x² -\frac 4 5 e^{-2.5x}`$.

		donc $`\int_{1}^2 f(x) dx = F(2) - F(1) = \frac 1 2 2² - \frac 4 5 e^{-2.5 \times 2}- ( \frac 1 2 1² - \frac 4 5 e^{-2.5 \times 1})= 1.560`$



​				L'approximation avait trouvé 1.476 et a donc commis l'erreur :

​				$` erreur = \frac {1.560 - 1.476} {1.560} \times 100 =5.38 `$%



				### Probabilités

1. * On a une XP de Bernoulli B(0.02) : un bloc peut être défectueux ($`D`$) ou pas ($`\overline D`$). 

	* On répète cette XP $`n`$ fois de façon identique et indépendant (car il y a remise) et , à chaque tirage, on associe la VA $`X`$ égale au nombre de succès.

		Donc $` X \rightarrow B(n , 0.02)`$

2. $`X \rightarrow B(360, 0.0.2)`$

	a. $`E(X) = np = 360 \times 0.02 =`$ 7.2. On peut espérer , sur un grand nombre de lots de 360 blocs, avoir en moyenne 7.2 blocs défectueux sur les 360.

	b.  $`p(X = 7) = `$0.1500

	c. $`p(X \geq 1) =`$0.9993

3. $` X \rightarrow B(3960, 0.02)`$

	a. La moyenne de la loi normale est $`E(X) = np = 3960 \times 0.02`$= 79.2

	​	L'écart-type de la loi normale est $`\sigma (X) = \sqrt {np(1-p)} =\sqrt {3960 \times 0.02  \times 0.98} `$ = 8.8

	b.  $`Z \rightarrow N(79.2, 8.8)`$

	​	$`p(Z \geq 89.5) =`$ 0.1209

	​	Il y a environ 12% de chance pour que le nombre de pièces défectueuses dépasse 89.5.

	

	**Partie B : Loi exponentielle **

	

	_Rappel : loi de durée de vie sans vieillissement_ 

	* le paramètre de la loi est $`\lambda`$ 
	* Si $`X`$ suit une loi exponentielle de paramètre $`\lambda`$ alors :
		* $`E(X) = \frac 1 \lambda`$ . Ainsi plus $`\lambda`$ est grand plus l'espérance est faible.
		* $`p(a \leq X \leq b) = \int_{a}^b \lambda e^{- \lambda x} dx = [- e ^{-\lambda x}]_{a}^b = -e^{- \lambda b} - - e^{- \lambda a} = e^{- \lambda a} - e^{- \lambda b}`$ 
		* $`p(X \leq b)  = 1 - e^{-\lambda b}`$ : cas particulier avec $`a = 0`$.

	1. $`p(T \leq 35000)= 1 - e^{-\lambda \times 35000}=1 - e^{-0.000011 \times 35000}=`$ 0.320
	2. $`E(X) = \frac 1 \lambda = \frac 1 {0.000011}=`$ 90909 heures.  Sur un grand nombre de blocs, en moyenne, l'espérance de vie est de 90909h à 1h près.

	

	**Partie C : intervalles de confiances**

	

	_Rappel :_

	Soit un échantillon représentatif de la population connu de taille $`n`$ connu et assez grand ($`n > 30`$) de moyenne $`\overline x`$ et d'écart-type $`\sigma`$_econnu ou pas...

	

	Objectif : trouver un intervalle dans lequel on a une chance $`\alpha`$ de trouver la moyenne $`\mu`$ de la population.

	* Si l'écart-type de la population est inconnue alors on prend pour **estimation ponctuelle** de l'écart-type : $`\sigma =  {\sigma_e} \sqrt \frac n {n-1}`$ .

	* On trouve le réel positif $`t`$ tel que $`p(-t \leq T \leq t ) = \alpha`$ avec $` T \rightarrow N(0,1)`$

		* Par exemple, si $`\alpha = 0.95`$, on chercher  le réel positif $`t`$ tel que $`p(-t \leq T \leq t ) = 0.95`$ avec $` T \rightarrow N(0,1)`$ : A la calculatrice on obtient $`t = 1.96`$

			

	* Il y a alors une probabilité $`\alpha`$ pour que la moyenne $`\mu`$ de la population soit dans l'**intervalle de confiance** :

		$`I = [\overline x - t \times \frac \sigma {\sqrt n}; \overline x + t \times \frac  \sigma {\sqrt n}]`$ 

		

	1. Pour l'échantillon :

		$`n = 100`$

		$`\overline x = 288`$

		$`\sigma_e = 168`$ heures

		 $`s =  {\sigma_e} \sqrt \frac n {n-1} = 168 \sqrt {\frac {100} {99} }`$=168.85

	2. Il y a 95% de chance pour que $`\mu \in I`$

		$`I = [\overline x - 1.96 \times \frac s {\sqrt n}; \overline x + 1.96 \times \frac  s {\sqrt n}]`$ 

		$`I = [288 - 1.96 \times \frac {168.85} {\sqrt {100}} ; 288 + 1.96 \times \frac {168.85} {\sqrt {100}}  ]`$

		$`I = [254,905 ; 321,095]`$

	3. Il y a 95% de chance pour que $`\mu \in I`$ donc il y 5% que $`\mu`$ ne soit pas dans $`I`$.

		On n'est donc pas certain que $` \mu \in I`$.

		