# Corrigé DM3

### Exercice 1

**A . Equation différentielle**

1. $`(E_0) : y'' +5y'+4y=0`$

	a. Equation réduite est $`r² +5r²+4=0`$

	$`\Delta = b²-4ac = 25-4 \times 4 \times 1 = 9`$

	$`r_1 = \frac {-b + \sqrt {\Delta}}{2a} = \frac {-5 + \sqrt {9}}{2 \times 1} = -1`$

	$`r_2 = \frac {-b - \sqrt {\Delta}}{2a} = \frac {-5 - \sqrt {9}}{2 \times 1} =-4`$

	b. La forme générale des solutions de $`(E_0)`$ est $`\lambda e^{-t} + \mu e^{-4t}`$ où $`\lambda \in \R`$ et $`\mu \in \R`$.

	

2.  C'est une question très difficile qui n'est plus posée de cette façon dans les examens BTS depuis 2 - 3 ans. Cependant, elle est toujours au programme ... 



On sait que $`f(t) = k_1 e^{-t} + k_2 e^{-4t} + \frac 5 2`$ est une solution de $`(E)`$ telle que $`f(0) = 5`$ et $`f'(0) = -1`$.

* $`f(0) = 5`$ donc :

  $`k_1 e^{-0} + k_2 e^{-4 \times 0} + \frac 5 2 = 5`$

  $`k_1 + k_2 = 5 - \frac 5 2`$

  $`k_1 + k_2 = \frac 5 2`$ : on a une équation à deux inconnues

  

* $`f'(0) = -1`$.

  * Commençons par dériver $`f`$ :

  	$`f'(x) = k_1 \times -1 e^{-t} + k_2 \times -4e^{-4t} + 0 = -k_1 e^{-t}-4k_2 e^{-4t}`$

  * $`f'(0) = -1`$ et que $`e^0 = 1`$ donc :

  * $`-k_1-4k_2 =-1`$

  	$`k_1 + 4 k_2 = 1`$

  	$`k_1 = - 4k_2 +1`$
  	
  	

* Je sais donc que $`k_1 + k_2 = \frac 5 2`$ et que $`k_1 = -4k_2 + 1`$. 

  * En remplaçant $`k_1`$ par $`-4k_2+1`$ dans la première équation, on obtient :

  	$`-4k_2 +1 + k_2 = \frac 5 2`$

  	$`-3k_2 = \frac 5 2 -1`$

  	$`-3k_2 = \frac 3 2`$

  	$`k_2 = -\frac 1 2`$

  	

  *  $`k_1 = -4k_2 +1 = -4 \times \frac {-1} 2 +1 =3 `$

  

  > $`f(x)= 3 e^{-t} - \frac 1 2 e^{-4t} + \frac 5 2`$
  >
  > Remarquez qu'on tombe sur la fonction de la partie B.

  

  **Partie B**

  

  1. a.

  	Il semble que $`f`$ soit décroissante de 5 m à 2,5 m sur $`[0; + \infty[`$

  	b. 

  	$`f'(t) = -e^{-4t} (3 e^{3t} -2)`$

  	* on sait que $`3e^{3t} −2 > 0 `$ sur $`[0; + \infty[`$
  	* or $`e^{-4t} >0`$ sur $`\R`$  donc $`-e^{-4t}<0`$
  	* Donc, sur $`[0; + \infty[`$ , $`f'(x)<0`$ et $`f`$ est bien décroissante.

2. a. $`f(t) = 5 - t - \frac 5 2 t^2 + t^2 \epsilon(t)`$  avec $`\lim \limits_{ x \to {+ \infty}}   \epsilon(x) = 0`$

	b. $`(T) : y = 5 - t`$

	c. Le termine qui suit $`5-t`$ est $`- \frac 5 2 x²`$ qui est négatif au voisinage de 0. Donc à partir de la tangente, on **enlève** quelque chose pour se rapprocher de la courbe $`C`$ : **$`C`$ est en dessous en $`T`$**



3. $`f(x)= 3 e^{-t} - \frac 1 2 e^{-4t} + \frac 5 2`$

	a. 

	* $`\lim \limits_{x \to + \infty} 3 e^{-t} = 0`$

	* $`\lim \limits_{x \to +\infty}- \frac 1 2 e^{-4t} =0`$

		donc $`\lim \limits_{x to +\infty}f(x) = \frac 5 2`$

	b. 

	* $`(D) : y = \frac 5 2`$ est asymptote à $`C`$ en $`+ \infty`$
	* le plateau descend donc en se rapprochant de 2,5 m

	

**Partie C**

|       | étape 1 | étape 2 | étape 3 | étape 4 | étape 5 |
| ----- | ------- | ------- | ------- | ------- | ------- |
| a     | 5       | 5.5     | 5.5     | 5.625   | 5.6875  |
| b     | 6       | 6       | 5.75    | 5.75    | 5.75    |
| b - a | 1       | 0.5     | 0.25    | 0.125   | 0.0625  |
| m     | 5.5     | 5.75    | 5.625   | 5.6875  | 5.17875 |
| f(m)  | 2.512   | 2.509   | 2.5108  | 2.51016 | 2.5098  |



* A l'étape 5, la condition de fin $`b - a < 0,1`$ est atteinte.
* L'algorithme affiche $`a`$ et $`b`$ c'est à dire 5,6875 et 5,75.
* Le plateau atteindre la hauteur souhaitée entre 5,6875s et 5,75s











