import module_liste

def lire_donnees(nom_fichier):
    '''
    renvoie une file contentant les caractères du texte du fichier text passé en paramètre
    : param nom_fichier
    type str
    : return
    type File
    '''
    assert type(nom_fichier) is str,'nom_fichier is str'
    try :
        lecture = open(nom_fichier, 'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
        lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
        lecture.close()        
    except FileNotFoundError :
        raise Exception('nom de fichier incorrect')
    return lignes

def convertir_en_file(lignes):
    '''
    renvoie une file contenant tous les caractères de la liste
    un elément de la file est un caractère unique
    : param lignes
    typr list of str
    : return File
    >>> liste = ['abc', 'd', 'ef']
    >>> test_file = convertir_en_file(liste)
    >>> print(test_file)
    (a, (b, (c, (d, (e, (f, ()))))))

    '''
    ma_file = module_liste.File()
    for ligne in lignes :
        for caractere in ligne :
            ma_file.enfile(caractere)
    return ma_file

def sauver(lignes, nom_fichier):
    '''
    sauvegarde la lignes sous le nom stipulé
    : param lignes
    type list of str
    : param nom_fichier
    type str
    '''
    try :
        ecriture = open(nom_fichier,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(lignes)
        ecriture.close()
    except:
        raise Exception('nom de fichier incorrect')

def convertir_en_ligne(ma_file):
    '''
    renvoie une liste dont le seul élément est la chaîne
    des caractères concaténés de la file
    : param ma_file
    type File
    return list of str
    >>> liste = ['abc', 'd', 'ef']
    >>> test_file = convertir_en_file(liste)
    >>> convertir_en_ligne(test_file)
    ['abcdef']
    '''
    chaine = ''
    while not ma_file.est_vide():
        chaine += ma_file.defile()
    return [chaine]
        
    
          


def corriger(nom_fichier):
    '''
    charge, modifie puis sauvegarde le contenu du fichier nom_fichier afin d'utiliser
    la syntaxe Latex compatible avec un GIT
    : param nom_fichier
    type str
    : pas de return
    : sauvegarde un nouveau fichier 
    '''
    ma_file = convertir_en_file(lire_donnees(nom_fichier))
    file_corrigee = modifier_syntaxe(ma_file)
    lignes = convertir_en_ligne(file_corrigee)
    sauver(lignes, nom_fichier[:-3]+'_corrige.md')
    

def modifier_syntaxe(ma_file):
    '''
    renvoie une chaine contenant les caractères de ma_file
    en modifiant les codes en latex
    : param ma_file
    type File
    : return
    type str
  
    '''
    file_corrigee = module_liste.File()
    while not ma_file.est_vide():
        caractere = ma_file.defile()
        if caractere == '$' :
            code = extraire_code(ma_file)
            code = ajouter_anti_cotes(code)
            file_corrigee.enfile(code)
        else :
            file_corrigee.enfile(caractere)
    return file_corrigee

def extraire_code(ma_file):
    '''
    renvoie une chaine de caractère conteant le code entre deux dollars. elimine les éventuels
    dollars doublons.
    : param ma_file
    type File
    : return
    str
    >>> ma_file = convertir_en_file(lire_donnees('test1.md'))
    >>> chaine = extraire_code(ma_file)
    >>> print(chaine)
    $\\frac 1 3 \\times \\frac 3 4$
    >>> ma_file = convertir_en_file(lire_donnees('test2.md'))
    >>> chaine = extraire_code(ma_file)
    >>> print(chaine)
    $\\frac 1 3 \\times \\frac 3 4$
    
    '''
    chaine = '$' # on vient de lire un dollar
    #test du double dollar
    double_dollar = False
    caractere = ma_file.defile()
    if caractere == '$' :
        double_dollar = True
        caractere = ''
    else :
        chaine += caractere
    # on cherche le dollar suivant
    while not caractere == '$':
        caractere = ma_file.defile()
        chaine += caractere
    # si double dollar on défile une fois pour l'enlever
    if double_dollar :
        ma_file.defile()
    return chaine

def ajouter_anti_cotes(code):
    '''
    renvoie la chaine code en y ajoutant, si necessaire, les anti-cotes
    : param code
    type str
    : return
    type str
    >>> ma_file = convertir_en_file(lire_donnees('test1.md'))
    >>> code = extraire_code(ma_file)
    >>> code_corrige = ajouter_anti_cotes(code)
    >>> print(code_corrige)
    $`\\frac 1 3 \\times \\frac 3 4`$
    >>> ma_file = convertir_en_file(lire_donnees('test2.md'))
    >>> code = extraire_code(ma_file)
    >>> code_corrige = ajouter_anti_cotes(code)
    >>> print(code_corrige)
    $`\\frac 1 3 \\times \\frac 3 4`$
    >>> ma_file = convertir_en_file(lire_donnees('test3.md'))
    >>> code = extraire_code(ma_file)
    >>> code_corrige = ajouter_anti_cotes(code)
    >>> print(code_corrige)
    $`\\frac 1 3 \\times \\frac 3 4`$
    
    '''
    if code[1] != '`':
        code = '$`' + code[1:-1] + '`$'
    return code








########################################
if __name__ == "__main__" :
    import doctest
    doctest.testmod(verbose = False)
    