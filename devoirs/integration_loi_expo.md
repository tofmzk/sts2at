# Intégration et loi exponentielle 

### Exercice 1 : Intégration et aires

Soit  $`f(x) = 2x+1`$ 

1. Calculer $`I = \int_0^1 {2 x +1 ~dx }`$ en utilisant une primitive de $`f(x)`$ à déterminer.
2. Tracer la représentation graphique de la fonction pour $`x \in [0;2]`$ dans un repère orthonormé.
3. Utiliser la représentation graphique pour calculer $`I`$ d'une autre façon. 

### Exercice 2 : Intégration et polynômes

1. Déterminer une primitive $`F(x)`$ de $`f(x)=6x²+4x-2`$ sur $`\R`$.

2. En déduire  $`\int_{-1}^1 {~f(x) dx }`$ 


### Exercice 3 : Loi exponentielle.

Soit $`X`$ une variable aléatoire qui suit la loi exponentielle de paramètre  $`\lambda = 0.01`$.

1. Quelle est la densité de cette loi ?

2. calculer, en indiquant vos calculs et en arrondissant à $`10^{-2}`$ près.

    a. $`p(X \leq 100)`$

    b. $`p(100 \leq X \leq 300)`$

    c. $`p_{X \geq 100}(X \leq 300)`$

3. Expliquer la différence de sens entre les résultats 2b. et 2c.

### Exercice 4 : Loi de durée de vie sans vieillissement.

Le carbone 14 a une demi-vie de $`t_{0,5} = 5730~ans`$. On peut modéliser la durée de vie $`X`$ (en année) des atomes de carbone 14 par une loi exponentielle de paramètre $`\lambda = \frac {ln(2)}{t_{0,5}}`$

1. Expliquer pourquoi $`p(X \leq 5730) = 0.5`$.
2. En déduire $`p_{X \geq 5730}(X \leq 11460)`$ et $`p_{X \geq 11460}(X \leq 17190)`$. Expliquer.
3. On trouve un fossile qui ne contient que 12,5% du carbone 14 initial. Donner une approximation de son âge.