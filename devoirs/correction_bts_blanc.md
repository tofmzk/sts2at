# Correction BTS blanc

Sur 24 ramené sur 20.



## Exercice 1 (13 pts)



### Partie A (4 pts)

1. Chaque créneau a la même probabilité d'être réalisé et $`X \in [0;20]`$ donc $`X \rightarrow U(0; 20)`$.
2. $`p(X \leq 5) = \frac 5 {20} = \frac 1 4`$
3. $`E(X) = \frac {0 + 20 } 2 = 10`$ : Sur un grand nombre de jours, on peut espérer un retard moyen de 10 min.
4. $`p_{X \geq 10}(X \leq 15) = \frac {p(10 \leq X \leq 15)} {p(X \geq 10)} = \frac {\frac {5}{20}}{\frac {10}{20}} = \frac 5 {10} = \frac 1 2`$

### Partie B (3 pts)

1. a. $`p(T \leq a) = \int_0^a {\lambda e^{-\lambda x} dx}= [-e^{-\lambda x}]_0^a = -e^{-\lambda \times a} - - e^{-\lambda \times 0} = -e^{-\lambda a} + 1 = 1 -e^{-\lambda a}`$

    b. $`p(T \leq 1000) = 1 - e^{-0.0002 \times 1000} \approx 0.18`$

2. $`E(X) = \frac 1 \lambda = 5000h`$



### Partie C (2 pts)

Une bille est conforme si et seulement si $`D \in [9,98; 10,02]`$.

$`p(9,98 \leq D \leq 10, 02) = 0, 95`$

Ainsi, la probabilité qu'une bille soit non conforme est 1 - 0,95 = **0,05**.



### Partie D (4 pts)

1. * Une bille peut être conforme ou non : il s'agit d'une expérience de Bernoulli B(0,05)
    * On répète cette expérience 100 fois de façon identique et indépendante (car il y a remise) .
    * On chaque série de 100 expériences, on associe la variable aléatoire Y égale au nombre de succès
    * Ainsi $`Y \rightarrow B(100; 0.05)`$

2. a. $`p(Y = 1) = 0.03`$

    b. $`p(Y \geq 1) = 0.99`$

3. a. On a bien $`n \geq 30`$ et $`p \leq 0.1`$ donc Y suit approximativement la loi de Poisson de paramètre $`\lambda = n p = 5`$

    b. $`p(Y \leq 2) = 0.12`$



## Exercice 2 (11 pts)

### Partie A (5 pts)

1. * $`g(x) = \frac b a = \frac {0.3} 1 = 0.3`$
    * une primitive de $`g`$ est $`G(t) = 0.3t`$
    * La forme générale des solutions de $`(E_0)`$ est $`Ke^{-0.3t}~où~K \in \R`$

2. $`h' + 0,3h = (12)' + 0.3 \times 12 = 0 + 3,6 = 3,6`$ donc $`h(t) = 12`$ est une solution particulière de (E).

3. La forme générale des solutions de (E) est  $`Ke^{-0.3t}+12~où~K \in \R`$.

4. on cherche une solution de la forme  $`f(t)=Ke^{-0.3t}+12`$ avec $`f(0) = 2`$

    En remplaçant $`t`$ par 0, il vient $`Ke^0 + 12 = 2`$ et donc $`K = -10`$.

    L'unique solution recherchée est donc $`f(t) = -10e^{-0.3 t} + 12`$



### Partie B ( 4 pts)

1. $`f(4) \approx = 8.99 m`$
2. D'après le logiciel de calcul formel (on aura reconnu `Géogèbra`), on peut affirmer que $`\lim_\limits{t \to +\infty} f(t) = 12`$ donc la droite$`\Delta`$ d'équation y = 12 est asymptote horizontale en +oo.

3. $`f'(0) = 4e^0  = 3 = \frac 3 1= \frac {variation~verticale}{variation~horizontale}`$

    Ainsi, à l'instant 0, $`f`$ varie de 3 m chaque seconde : Il s'agit de la vitesse initiale : 3 m/s.

4. $`f'(t) = 3 e^{-0.3t}`$. Une exponentielle étant strictement positive sur $`\R`$, $`f'`$ l'est également. Par conséquent, $`f`$ f est strictement croissante sur $`[0; +\infty]`$de $`f(0) = 2`$ à $`+\infty`$.

5. On doit voir  sur le graphique :
    * l'asymptote $`\Delta : y = 12`$ (cf 2)
    *  la tangente en (0; 2) qui a pour pente 3 (cf 3)
    * Le point particulier (4; 9) (cf 1)
    * la courbe évidemment

![graphique](graphique_sujet1.jpg)

### Partie C (2 pts)

 

|          |  t   | f(t)  | condition | affichage |
| :------: | :--: | :---: | :-------: | :-------: |
| étape 1  |  0   |   2   |   vraie   |   rien    |
| étape 2  |  1   | 4.59  |   vraie   |   rien    |
| etc ...  |      |       |           |           |
| étape 14 |  13  | 11.80 |   vraie   |   rien    |
| étape 15 |  14  | 11.85 |   vraie   |   rien    |
| étape 16 |  15  | 11.89 |   vraie   |   rien    |
| étape 17 |  16  | 11.92 |  fausse   |    16     |
|          |      |       |           |           |

Il faudra attendre 16 s pour que la nacelle s'élève à la hauteur souhaitée.



