# Correction intégration et loi exponentielle



## Exercice 1 (2 + 2 + 1 = 5pts)

1. Une primitive de $`f(x)=2x+1`$ est $`F(x) = x²+x`$.

    $`\int_0^1{f(x) dx} = F(1) - F(0) = 1²+1 - 0 = 2`$

2. <img src="media/f=2x+1.jpg" alt="representation graphique" style="zoom:50%;" />

3. Comme $`f`$ est positive sur [0; 1], l'intégrale est l'aire entre la courbe, l'axe des abscisses et les droites $`x=0`$ et $`x= 1.`$

    Cette aire vaut : $`\frac {(1+3) \times 1} 2 = 2`$

    On retrouve bien le résultat du 1.

## Exercice 2 (2 + 2 = 4 pts)

1. $`f(x) = 6x²+4x-2`$

    $`F(x) = 6 \times \frac 1 3 x^3 + 4 \times \frac 1 2 x² - 2 \times x = 2x³+2x²-2x`$

2. $`\int_{-1}^1{f(x) dx} = F(1) - F(-1) = 2\times 1³ + 2 \times 1²-2 \times 1 - (2 \times (-1)³+2\times (-1)²-2 \times (-1) ) = 2 - 2 = 0  `$



## Exercice 3 (1+ 1.5+ 1.5 +1.5 + 1.5 = 7 pts)

1. $`f(x) = 0.01 e^{-0.01x}`$

2. Une primitive de $`f`$ est $`F(x)= - e^{-0.01x}`$ 

3. a. $`p(X \leq 100) = F(100) - F(0) = -e^{-0.01 \times 100} -- 1 = 1- e^{-1}  \approx 0.63`$

    b. $`p(100 \leq X \leq 300 ) = F(300) - F(100) = -e^{-0.01 \times 300} - -e^{-0.01 \times 100} =  e^{-1} - e^{-2} \approx 0.32`$

    c. $`p_{X \geq 100}(X \leq 300) =\frac {p(100 \leq X \leq 300)}{p(X \geq 100)} \approx \frac {0.32}{1 - 0.63} = 0.86`$

4. Dans la première, on n'a aucune indication concernant $`X`$ tandis que dans la seconde, on sait à l'avance que $`X \geq 100`$.

## Exercice 4 ( 1 + 1.5 + 1.5 = 4 pts)

1. On sait que , tous les 5730 ans, 50% des atomes restant sont désintégrés. Il y a donc une chance sur deux qu'un atome donné soit détruit entre 0 et 5730 ans.

2. Ces deux probabilités valent toutes les deux 0.5 car la loi exponentielle est la loi de durée de vie sans vieillissement. Autrement dit, la probabilité de dépend pas de l'âge de l'atome au début de l'intervalle mais de l'amplitude de cet intervalle qui est, dans les deux cas, 5730 ans, comme à la première question.

3. pour passer de 100% à 12,5%, il faut diviser par 2 trois fois de suite :

    $\frac {100} 2 = 50`$

    $\frac {50} 2 = 25`$

    $\frac {25} 2 = 12.5`$

    Il faut donc passer trois demi-vies soit $`3 \times 5730 = 17190ans`$



