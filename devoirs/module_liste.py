class Maillon :
    '''
    une classe pour construire un maillon d'nue liste chainee
    '''
    def __init__(self, valeur = None, suivant = None) :
        '''
        construit un maillon avec une valeur et un lien vers le maillon suivant.
        : param valeur
        type variable
        : param suivant
        type Maillon        
        '''
        self.valeur = valeur
        self.suivant = suivant
    
       
        

class Liste_chainee :
    '''
    une classe pour implémenter une liste chainee
    '''
    def __init__(self):
        '''
        construit une liste chainee vide de longueur 0
        '''
        self.tete = None
        self.longueur = 0
        
    def renvoie_maillon(self, indice) :
        '''
        renvoie le maillon d'indice n
        : param n
        type integer
        return un maillon
        type Maillon
        '''
        assert(type(indice) == int and indice >= 0 and indice < self.longueur), 'indice incorrect'
        maillon = self.tete
        for _ in range(indice) :
            maillon = maillon.suivant
        return maillon
            
            
    def ajouter(self, valeur):
        '''
        ajoute un maillon de valeur précisé en tête de liste
        : param valeur
        type ???
        '''
        maillon = Maillon(valeur, self.tete)
        self.tete = maillon
        self.longueur += 1
    
    def est_vide(self):
        '''
        renvoie True si la liste est vide et False sinon
         >>> l = Liste_chainee()
         >>> l.est_vide()
         True
         >>> l.ajouter(1)
         >>> l.est_vide()
         False
        '''
        return self.tete == None
    
    def defile(self):
        if not self.est_vide():
            valeur = self.tete.valeur
            self.tete = self.tete.suivant
            self.longueur -= 1
            return valeur
        else :
            raise 'impossible de défiler une liste vide'
    
    def inserer(self, indice, valeur):
        '''
        insère un nouveau maillon à l'indice stipulé, de valeur précisée
        : param indice
        type int
        : param valeur
        type ?
        >>> l = Liste_chainee()
        >>> l.inserer(0, 1)
        >>> l.inserer(0, 2)
        >>> l.inserer(2, 3)
        >>> print(l)
        (2, (1, (3, ())))
        '''
        assert(isinstance(indice, int) and indice >= 0 and indice <= self.longueur), 'indice est un entier positif inférieur ou égal à la longueur de la liste'
        self.longueur += 1
        if indice > 0 : # si on insère pas en tête
            maillon_precedent = self.renvoie_maillon(indice - 1)
            maillon_suivant = maillon_precedent.suivant
            nouveau_maillon = Maillon(valeur, maillon_suivant)
            maillon_precedent.suivant = nouveau_maillon
            
        else :
            nouveau_maillon = Maillon(valeur, self.tete)
            self.tete = nouveau_maillon
    
    def supprimer(self, indice):
        '''
        supprime un nouveau maillon à l'indice stipulé, de valeur précisée
        : param indice
        type int
        >>> l = Liste_chainee()
        >>> l.inserer(0, 1)
        >>> l.inserer(0, 2)
        >>> l.inserer(2, 3)
        >>> l.supprimer(1)
        >>> print(l)
        (2, (3, ()))
        '''
        assert(isinstance(indice, int) and indice >= 0 and indice < self.longueur), 'indice est un entier positif inférieur ou égal à la longueur de la liste'
        self.longueur -= 1
        if indice > 0 : # si on supprime pas en tête
            maillon_precedent = self.renvoie_maillon(indice - 1)
            maillon_suivant = maillon_precedent.suivant.suivant
            maillon_precedent.suivant = maillon_suivant
            
        else :
            self.tete = self.tete.suivant
            
    def __getitem__(self, indice):
        '''
        renvoie la valeur du maillon d'indice précisé
        : param indice
        type int
        return la valeur du maillon d'indice n
        type ??
        '''
        assert(isinstance(indice, int) and indice >= 0 and indice < self.longueur), 'indice est un entier positif inférieur strictement à la longueur de la liste'
        maillon = self.renvoie_maillon(indice)
        return maillon.valeur
    
    def __setitem__(self, indice, valeur):
        '''
        remplace la valeur du maillon d'indice précisé par celle passée en paramètre
        : param indice
        type int
        return la valeur du maillon d'indice n
        type ??
        '''
        assert(isinstance(indice, int) and indice >= 0 and indice < self.longueur), 'indice est un entier positif inférieur strictement à la longueur de la liste'
        maillon = self.renvoie_maillon(indice)
        maillon.valeur = valeur
    
    def __len__(self):
        return self.longueur
    
            
    def __str__(self):
        '''
        renvoie une chaine pour visualiser la liste
        : return
        type str
        >>> l = Liste_chainee()
        >>> l.ajouter(1)
        >>> print(l)
        (1, ())
        >>> l.ajouter(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine

    def __repr__(self):
        '''
        donne la classe de l'objet et sa taille
        '''
        return 'Class Liste_chainées de taille ' + str(self.longueur)
 
 
class Pile:
    '''
    une classe pour modéliser une pile
    '''
    def __init__(self):
        '''
        construit une pile vide
        '''
        self.tete = None
        self.longueur = 0
    
    def est_vide(self):
        '''
        renvoie True si la pile est vide et False sinon
        : return
        boolean
        >>> p = Pile()
        >>> p.est_vide()
        True
        >>> p.empile(1)
        >>> p.est_vide()
        False
        '''
        return self.longueur == 0
    
    def empile(self, valeur):
        '''
        ajoute la valeur en tête de pile
        >>> p = Pile()
        >>> p.empile(1)
        >>> p.empile(2)
        >>> p.empile('a')
        >>> print(p)
        (a, (2, (1, ())))
        
        '''
        maillon = Maillon(valeur, self.tete)
        self.tete = maillon
        self.longueur += 1
        
    def depile(self):
        '''
        enlève la valeur en tête de pile et la renvoie
        : return
        type ?
        >>> p = Pile()
        >>> p.empile(1)
        >>> p.empile(2)
        >>> p.empile('a')
        >>> p.depile()
        'a'
        >>> print(p)
        (2, (1, ()))
        '''
        if not self.est_vide() :
            valeur = self.tete.valeur
            self.tete = self.tete.suivant
            self.longueur -= 1
            return valeur
        else :
            raise "impossible de dépiler une pile vide"
        
    def __str__(self):
        '''
        renvoie une chaine pour visualiser la pile
        : return
        type str
        >>> l = Pile()
        >>> l.empile(1)
        >>> print(l)
        (1, ())
        >>> l.empile(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine
        
        
    def __repr__(self):
        '''
        renvoie un chaine qui décrit la classe
        '''
        return 'Classe Pile'
        
        
class File:
    '''
    une classe pour les Files
    '''
    def __init__(self):
        '''
        contruit une file vide de longueur 0
        '''
        self.tete = None
        self.queue = None
        self.longueur = 0
    
    def est_vide(self):
        '''
        renvoie True si la file est vide et False sinon
        >>> f = File()
        >>> f.est_vide()
        True
        '''
        return self.longueur == 0
    
    def enfile(self, valeur):
        '''
        ajoute la valeur en queue de file
        : param valeur la valeur a ajouter
        type ?
        >>> f = File()
        >>> f.enfile(3)
        >>> f.est_vide()
        False
        '''
        maillon = Maillon(valeur, None)
        if self.tete == None :
            self.tete = maillon            
        elif self.longueur == 1 :
            self.tete.suivant = maillon                 
        else :
            self.queue.suivant = maillon
        self.queue = maillon
        self.longueur += 1
        
        
    def defile(self):
        '''
        renvoie la valeur en tête de file et la retire de la file
        : return valeur
        type ?
        >>> f = File()
        >>> f.enfile(3)
        >>> f.enfile(2)
        >>> f.defile()
        3
        >>> f.defile()
        2
        >>> f.est_vide()
        True
        '''
        if self.tete != None :
            tete = self.tete.valeur
            self.tete = self.tete.suivant
            self.longueur -= 1
            return tete
        else :
            raise Exception('On ne défile pas une file vide')
    
    def __str__(self):
        '''
        renvoie la chaine permettant d'afficher la file
        : return
        str
        >>> f = File()
        >>> f.enfile(3)
        >>> f.enfile(2)
        >>> f.enfile('a')
        >>> print(f)
        (3, (2, (a, ())))
        
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            for _ in range(self.longueur) :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
            for _ in range(self.longueur):
                chaine += ')'
        
        chaine += ')'
        return chaine
    
    def __repr__(self):
        '''
        renvoie une représentation de la file
        '''
        return 'classe FILE'
            
     
 
 
 
 
 
 
 
 
 
 
            
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True)            
        
