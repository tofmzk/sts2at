# Corrigé complet du Sujet 4



### Algèbre

#### partie A

1. $$E_0 : y'' - y' -2y =0$$

	* équation caractéristique : $$r² - r - 2 = 0$$

		$$a = 1, b=-1, c= -1$$

		* $$\Delta = b²-4ac = (-1)²-4 \times 1 \times -2 = 1 + 8 = 9$$

			$$x_1 = \frac {-b + \sqrt \Delta}{2a}= \frac {1 + 3}{2} = 2$$

			$$x_2 = \frac {-b - \sqrt \Delta}{2a}= \frac {1 - 3}{2} = -1$$

			

	* La forme générale des solutions de (E~0~) est : $$\lambda e^{2x} + \mu e^{-1x}$$ où $$\lambda \in \R$$ et $$\mu \in \R$$

	

2. a. $$h(x)= (x² +2x)e^{-x}$$

    

    _Rappels_ :

    $$(b)' = 0$$

    $$(ax)' = a$$

    $$(x²) = 2x$$

    $$e^{ax} = ae^{ax}$$

    $$ (uv)' = u'v + v'u$$ avec $$u = (x²+2x)$$ et $$v=e^{-x}$$

    

    $$ h'(x)= (x² + 2x)'e^{-x} + (e^{-x})'(x²+2x)$$

    $$h'(x)= (2x+2)e^{-x} - e^{-x} \times (x²+2x)$$

    $$h'(x) = (2x+2 - (x²+2x))e^{-x}$$

    $$h'(x) = (2x+2-x²-2x)e^{-x}$$

    $$h'(x)= (2-x²)e^{-x}$$

    

    b. 

    (E) : $$y'' - y' - 2y = (-6x-4)e^{-x}$$

    On va remplacer $$y$$ par $$h$$ dans la partie de gauche : va-t-on trouver celle de droite ?

    $$h(x) = (x²+2x)e^{-x}$$

    $$h'(x)=(2-x²)e^{-x}$$

    $$h''(x)= (x²-2x-2)e^{-x}$$



​		$$h'' - h' - h = (x²-2x-2)e^{-x} - (2-x²)e^{-x} - 2(x²+2x)e^{-x}$$

​		$$ h'' - h' - h = e^{-x}(x²-2x-2 - 2 + x² -2x² -4x)$$

​		$$h''-h'-h = e^{-x}(-6x -4)$$



​		$$h$$ vérifie l'équation (E) donc $$h(x) = (x²+2x)e^{-x}$$ est une solution particulière de (E)



3. La forme générale des solutions de (E) est :

	$$\lambda e^{2x} + \mu e^{-1x} + (x²+2x)e^{-x}$$  où $$\lambda \in \R$$ et $$\mu \in \R$$





4.    

* On admet que $$f(x) = (x+1)² e^{-x}$$ est une solution de (E) : inutile de le prouver !
* Le logiciel de calcul formel nous dit que $$f'(x) = ((x+1)² e^{-x})'=2(x+1)e^{-x} - (x+1)²e^{-x}$$ : inutile de le re-calculer !



​				La seule question est : les conditions initiales sont-elles vérifiées ?

​				$$f(0)= (0+1)²e^{-0}=1 \times 1 = 1$$ : la première condition est vérifiée

​				$$f'(0) = 2(0+1) e^{0}-(0+1) e^0 = 2 - 1 = 1$$ ; la deuxième condition est vérifiée



$$f$$ est bien la solution recherchée.



#### partie B

 $$f(x) = (x+1)² e^{-x}$$ 



1. $$\lim\limits_{x \to +\infty} f(x) = 0$$

	$$\lim\limits_{x \to -\infty} f(x) = +\infty$$



​		_Pourquoi ?_

​			$$\lim\limits_{x \to +\infty} e^{-x} = 0$$

​			$$\lim\limits_{x \to -\infty} e^{-x} = + \infty$$

​				Comme entre un polynôme et une fonction exponentielle il y a une indétermination, c'est 			l'exponentielle qui l'emporte à l'infini ...



2. a. on sait $$f'(x) = ((x+1)² e^{-x})'=2(x+1)e^{-x} - (x+1)²e^{-x}$$ 

    $$ f'(x)= e^{-x}(2(x+1)-(x+1)²)$$

    $$f'(x)=e^{-x}(2x+2 - (x²+2x+1))$$

    $$f'(x) = e^{-x}(2x+2 -x² -2x - 1)$$

    $$f'(x)=e^{-x}(1-x²)$$

    

    b. $$1 - x²$$ est un polynôme de degré 2 

    *  $$1 - x² = (1-x)(1+x)$$ : s'annule en 1 et en -1  

    *  $$e^{-x}$$ est toujours positif ... plus grand chose à faire

    |       | -oo                                            -1                          1                                                                        +oo |
    | ----- | ------------------------------------------------------------ |
    | f'(x) | -                                  0            +            0                                   - |

    

    $$f$$ est décroissante sur ]-oo; -1], croissante sur [-1;1] puis décroissante sur [1; +oo]

    

    c.

3. C'est du cours : 

    * le $$DL_4(0)$$ de $$f$$ est $$f(x) =1 + x - \frac {x²}{2} - \frac {x³}{ 9} + 5 \times \frac {x^4}{ 24} + x^4\epsilon (x) $$ où $$\lim\limits_{x \to 0} \epsilon (x) = 0$$

    * $$y= 1 + x$$ la partie de degré 1 du $$DL_4(0)$$ donne l'équation de la tangente en 0
    * le signe du monôme qui suit l'équation (c'est à dire $$- \frac {x²}{2}$$ )permet de savoir qui de la courbe ou de la tangente est au dessus de l'autre.

    

    |                    | 0                                                            |
    | ------------------ | ------------------------------------------------------------ |
    | $$-\frac {x²}{2}$$ | **-**                                             0                                                      **-** |



La courbe de $$f$$  est toujours sous sa tangente autour de 0.



4. 

* Le logiciel de calcul formel nous dit que $$(-x²-4x-5)e^{x})' = (x+1)²e^{-x}$$ donc $$F(x)=(-x²-4x-5)e^{x}$$ est une primitive de $$f(x)=(x+1)²e^{-x}$$.
* du coup $$\int_{-1}^0 f(x) dx = F(0) - F(-1) = (-0²-4*0-5)e^0 - (-(-1)² -4 \times -1 -5)e^{--1} = -5+2e$$



## Proba

#### **partie A**



1. On fait un dessin de l'arbre et ça va tout seul  :

     <img src="proba_arbre.jpg" alt="arbre" style="zoom:30%;" />

    $$p(M_1) = 0,4$$ 

    $$p(M_2) = 0,6$$

    $$p_{M{1}} (D)=0,02$$ 

    $$p_{M2}(D)=0,98$$

    

2. a. 

    $$p(M_1 \cap D) = p(M_1) \times p_{M_1}(D) = 0,4 \times 0,02 = 0,008$$ (on multiplie les branches pour un intersection)

    $$p(M_2 \cap D) = p(M_2) \times p_{M_2}(D) = 0,6 \times 0,03 = 0,018$$ (on multiplie les branches pour un intersection)

    b.

    $$ D = (M_1 \cap D \cup M_2 \cap D)$$ avec $$M_1 \cap D$$ et $$ M_2 \cap D$$ disjoints donc :

    $$p(D) = p(M_1 \cap D) +p (M_2 \cap D) = .....$$



3. $$P_D(M_1) = \frac {p(M_1 \cap D)}{p(D)} = \frac{0,008} {0,026} = 0,308$$



#### **partie B**

1. On a un expérience de Bernoulli B(0,03) : une pièce peut être soit défectueuse soit non défectueuse.

    On répète cette expérience 50 fois de façon identique et indépendant et, à chaque expérience, on associe la variable aléatoire X égale au nombre de succès : X -> B(50; 0,03)

    

2. a. 

    $$p(X=0) = $$

    b.

     $$p( X \leq 2) =$$

    

3. a. 

    On a bien $$n>30$$ et $$p \leq 0,1$$ donc X suit approximativement la loi de Poisson de paramètre $$\lambda = n \times p = 1,5$$

    b.

    $$p(Y \leq 2) = $$

    

4. a.

    $$np = 1,5 < 5$$ on ne peut utiliser la loi Normale comme approximation de la loi Binomiale dans ce cas (pour appel, il faut $$np \geq 5$$ **et** $$n(1-p) \geq 5$$ **et** $$n \geq 30$$). L'élève a donc tort.

    b.

    La moyenne obtenue est $$\mu = E(X) = np = 1,5$$ et l'écart-type est  $$ \sigma = \sqrt { np(1-p)}$$ 

#### **partie C**



1. Z -> N(20;0,08)

    $$p(19,9 \leq Z \leq 20,1) =$$

    La probabilité que l'écrou soit non conforme est don $$1- ... = ...$$

2. Z -> N(20; $$\sigma$$). 

    on veut $$p(19,9 \leq Z \leq 20,1) =0,97$$

    $$p(\frac {19,9 - 20}{\sigma}\leq \frac {Z -20}{\sigma} \leq \frac {20,1 - 20}{\sigma}) = 0,97$$  

    $$p(\frac {-0,01}{\sigma} \leq T \leq \frac {0,01}{\sigma}) = 0,97$$ avec $$ T = \frac {Z -20}{\sigma} $$  et T-> N(0;1)

Faites un petit dessin représentant une courbe de Gauss :

* entre les deux bornes on a une aire de 0,97 : il reste donc 0,03 des deux côtés
* donc il y a $$\frac {0,03}{2} = 0,015$$ de chaque côté.
* avant $$\frac {0,01}{\sigma}$$ l'aire est donc de 0,97 + 0,015 = 0,985



On cherche donc $$\sigma $$ tel que $$p(T \leq  \frac {0,01}{\sigma}) = 0,985$$



a. L'intégrale calculée correspond à $$p(0 \leq T \leq a)$$ (il y a une coquille dans le sujet : on devrait lire 0,5 + $$\int_0^a ...$$

La boucle tourne tant que $$p(T \leq a)\leq0,985$$	: l'algo cherche à résoudre l'équation précédente de façon approchée à 0,01 près, puisque a avance de 0,01 en 0,01.

L'algo renvoie 2,14 donc a = 2,14 donc il reste à résoudre $$\frac {0,01}{\sigma} = 2,14$$ soit  $$\frac {0,01} { 2,14} = \sigma = .....$$



#### Partie D

1. a.

    $$ F \rightarrow N(p; \sqrt \frac{p(1-p)}{n}$$ : il suffit de remplacer p et n par leurs valeurs respectives ...

    b.

    $$p( F \geq 0,95) = 0.84$$

2. $$p( F \geq 0,95) =0,95$$.

3. $$p(\frac { F - 0,97}{\frac {0,17}{\sqrt n}} \geq \frac {0,95 - 0,97}{\frac {0,17}{\sqrt n}}) \geq 0,95$$

4. $$p( T \geq -0,02 \times \frac {\sqrt n}{0,17}) \geq 0,95$$  avec $$ T -> N(0;1)$$

    Grâce à la calculatrice : 

    $$-0,02 \times \frac {\sqrt n}{0,17} = -1.64$$

    donc $$ \sqrt n = \frac {0,17}{-0,02} \times -1.64 $$

    donc $$n = ( \frac {0,17}{-0,02} \times -1.64)² = 194$$

    

    Il faut environ 200 boulons pour arriver à ce que $$p( F \geq 0,95) =0,95$$.

    

    

    















